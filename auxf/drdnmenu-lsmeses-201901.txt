			<div class="tintimg"></div> 
 				<div style="float: left; width: 1%;"></div> 
  
 				<a href="https://klattrlog.bitbucket.io/"> <span class="tophdrtit-left">Collateral 
 						Commentary<br> <span style="padding-left: 11.5em;">Oubliette</span></span> </a> 
        <span class="kawabunga">  					
 
          <div class="dropdown"> <button class="dropbtn">2019</button> 
						<div class="dropdown-content"> 
   							<a href="../201902/kkm-201902p1.html">February 2019</a>
 								<a href="../201901/kkm-201901p1.html">January 2019</a>
              </div> 
  					</div> 

 					<div class="dropdown"> <button class="dropbtn">2018</button> 
						<div class="dropdown-content"> 
   											<a href="../201812/kkm-201812p1.html">December 2018</a> 
   											<a href="../201811/kkm-201811p1.html">November 2018</a> 
   											<a href="../201810/kkm-201810p1.html">October 2018</a> 
   											<a href="../201809/kkm-201809p1.html">September 2018</a> 
   											<a href="../201808/kkm-201808p1.html">August 2018</a> 
     						 <a href="../201807/kkm-201807p1.html">July 2018</a> 
     						 <a href="../201807/kkm-201807p1.html">July 2018</a> 
   						<a href="../201806/kkm-201806p1.html">June 2018</a> 
              <a href="../201805/kkm-201805p1.html">May 2018</a>
   						<a href="../201804/kkm-201804p1.html">April 2018</a>
              <a href="../201803/kkm-201803p1.html">March 2018</a>  
   						<a href="../201802/kkm-201802p1.html">February 2018</a> 
 						  <a href="../201801/kkm-201801p1.html">January 2018</a> </div> 
 					</div> 
 					<div class="dropdown"> <button class="dropbtn">2017</button> 
 						<div class="dropdown-content"> 
 						  <a href="../201712/kkm-201712p1.html">December 2017</a> 
 							<a href="../201711/kkm-201711p1.html">November 2017</a> 
 							<a href="../201710/kkm-201710p1.html">October 2017</a> 
 							<a href="../201709/kkm-201709p1.html">September 2017</a> 
 							<a href="../201708/kkm-201708p1.html">August 2017</a> 
 							<a href="../201707/kkm-201707p1.html">July 2017</a> 
 							<a href="../201706/kkm-201706p1.html">June 2017</a> 
 							<a href="../201705/kkm-201705p1.html">May 2017</a> 
 							<a href="../201704/kkm-201704p1.html">April 2017</a> 
 							<a href="../201703/kkm-201703p1.html">March 2017</a> 
 							<a href="../201702/kkm-201702p1.html">February 2017</a> 
 							<a href="../201701/kkm-201701p1.html">January 2017</a> 
 						 </div> 
 				    </div> 
 					<div class="dropdown"> <button class="dropbtn">2016</button> 
 						<div class="dropdown-content"><a href="../201612/kkm-201612p1.html">December 
 								2016</a> 
 							<a href="../201611/kkm-201611p1.html">November 2016</a> 
 							<a href="../201610/kkm-201610p1.html">October 2016</a> 
 							<a href="../201609/kkm-201609p1.html">September 2016</a> 
 							<a href="../201608/kkm-201608p1.html">August 2016</a> 
 							<a href="../201607/kkm-201607p1.html">July 2016</a> 
 							<a href="../201606/kkm-201606p1.html">June 2016</a> 
 							<a href="../201605/kkm-201605p1.html">May 2016</a> 
 							<a href="../201604/kkm-201604p1.html">April 2016</a> 
 							<a href="../201603/kkm-201603p1.html">March 2016</a> 
 							<a href="../201602/kkm-201602p1.html">February 2016</a> 
 							<a href="../201601/kkm-201601p1.html">January 2016</a> </div> 
 					</div> 
 					<div class="dropdown"> <button class="dropbtn">2015</button> 
 						<div class="dropdown-content"><a href="../201512/kkm-201512p1.html">December 
 								2015</a> 
 							<a href="../201511/kkm-201511p1.html">November 2015</a> 
 							<a href="../201510/kkm-201510p1.html">October 2015</a> 
 							<a href="../201509/kkm-201509p1.html">September 2015</a> 
 							<a href="../201508/kkm-201508p1.html">August 2015</a> 
 							<a href="../201507/kkm-201507p1.html">July 2015</a> 
 							<a href="../201506/kkm-201506p1.html">June 2015</a> 
 							<a href="../201505/kkm-201505p1.html">May 2015</a> 
 							<a href="../201504/kkm-201504p1.html">April 2015</a> 
 							<a href="../201503/kkm-201503p1.html">March 2015</a> 
 							<a href="../201502/kkm-201502p1.html">February 2015</a> 
 							<a href="../201501/kkm-201501p1.html">January 2015</a> </div> 
 					</div> 
 					<div class="dropdown"> <button class="dropbtn">2014</button> 
 						<div class="dropdown-content"><a href="../201412/kkm-201412p1.html">December 
 								2014</a> 
 							<a href="../201411/kkm-201411p1.html">November 2014</a> 
 							<a href="../201410/kkm-201410p1.html">October 2014</a> 
 							<a href="../201409/kkm-201409p1.html">September 2014</a> 
 							<a href="../201408/kkm-201408p1.html">August 2014</a> 
 							<a href="../201407/kkm-201407p1.html">July 2014</a> 
 							<a href="../201406/kkm-201406p1.html">June 2014</a> 
 							<a href="../201405/kkm-201405p1.html">May 2014</a> 
 							<a href="../201404/kkm-201404p1.html">April 2014</a> 
 							<a href="../201403/kkm-201403p1.html">March 2014</a> 
 							<a href="../201402/kkm-201402p1.html">February 2014</a> 
 							<a href="../201401/kkm-201401p1.html">January 2014</a> </div> 
 					</div> 
 					<div class="dropdown"> <button class="dropbtn">2013</button> 
 						<div class="dropdown-content"><a href="../201312/kkm-201312p1.html">December 
 								2013</a> 
 							<a href="../201311/kkm-201311p1.html">November 2013</a> 
 							<a href="../201310/kkm-201310p1.html">October 2013</a> 
 							<a href="../201309/kkm-201309p1.html">September 2013</a> 
 							<a href="../201308/kkm-201308p1.html">August 2013</a> 
 							<a href="../201307/kkm-201307p1.html">July 2013</a> 
 							<a href="../201306/kkm-201306p1.html">June 2013</a> 
 							<a href="../201305/kkm-201305p1.html">May 2013</a> 
 							<a href="../201304/kkm-201304p1.html">April 2013</a> 
 							<a href="../201303/kkm-201303p1.html">March 2013</a> 
 							<a href="../201302/kkm-201302p1.html">February 2013</a> </div> 
 					</div> 
 				</span> 
 				<div class="contenr"> <img class="riteclckpad" src="../auxf/hdrimg-em-75pc.jpg" width="5%">
