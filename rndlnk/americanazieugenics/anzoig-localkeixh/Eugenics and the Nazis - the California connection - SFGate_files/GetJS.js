treg.html={};
treg.html["gigya-screensets.html"]='\u003cdiv class="gya-hide treg-gya-service-unavailable" id=hearst-gigya-unavailable\u003e\u003ch2\u003eThe sign in service is not functioning right now.\u003c/h2\u003e\u003ch2\u003ePlease try again in a few minutes\u003c/h2\u003e\u003cp\u003eIf the issues continue, please contact our customer service at\u003cp\u003ePhone: \u003cspan class=treg-gya-supportdesk-phone\u003e\u003c/span\u003e\u003cp\u003eEmail: \u003cspan class=treg-gya-supportdesk-email\u003e\u003c/span\u003e\u003c/div\u003e\u003cdiv class="gigya-screen-set gya-container" id=hearst-registration-screen-set style=display:none data-on-pending-registration-screen=hearst-registration-screen-set/gigya-complete-registration-screen data-on-pending-verification-screen=hearst-registration-screen-set/gigya-verification-pending-screen data-on-pending-password-change-screen=hearst-registration-screen-set/gigya-password-change-required-screen data-on-existing-login-identifier-screen=hearst-link-accounts-screenset/gigya-link-account-screen\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-login-screen data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eSign in using\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003c/div\u003e\u003ch6 class=gya-rule\u003e\u003cspan\u003eor\u003c/span\u003e\u003c/h6\u003e\u003cform class=gigya-login-form\u003e\u003cinput class=gya-txt style=width:100% name=loginID formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput class=gya-txt type=password name=password style=width:100% placeholder=Password\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-login-form data-error-codes=403042 data-scope=bound-object-error\u003eEmail Address or Password is incorrect.\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003ePassword field is required.\u003cdiv class=gya-vcenter\u003e\u003cdiv class=gya-left\u003e\u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003ca data-switch-screen=gigya-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot password?\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter style=clear:both\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eKeep me signed in\u003c/label\u003e\u003c/div\u003e\u003cinput class=gya-btn style=width:100% type=submit value="Sign in"\u003e\u003c/form\u003e\u003cdiv class="treg-conditional treg-no-profile-editor"\u003e\u003ch6 class="treg-conditional treg-is-free gya-vcenter"\u003eDon\u0027t have a profile?\u003c/h6\u003e\u003ch6 class="treg-conditional treg-is-premium gya-vcenter"\u003eDon\u0027t have a subscription?\u003c/h6\u003e\u003ca class="treg-conditional treg-is-free gya-btn" data-switch-screen=gigya-register-screen\u003eSign up\u003c/a\u003e \u003ca class="treg-conditional treg-is-premium treg-gya-subscribe-link gya-btn" href=#\u003eSubscribe\u003c/a\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003eBy signing in, you agree to our \u003ca class=treg-gya-termsofservice-link\u003eTerms of Service\u003c/a\u003e and \u003ca class=treg-gya-privacy-link\u003ePrivacy Policy\u003c/a\u003e.\u003c/div\u003e\u003c/div\u003e\u003cdiv class="treg-conditional treg-gya-onprofile-page"\u003e\u003cdiv class="treg-conditional treg-is-free"\u003e\u003ch6 class="treg-conditional treg-is-free gya-vcenter"\u003eDon\u0027t have a profile?\u003c/h6\u003e\u003ca class="treg-conditional treg-is-free gya-btn" data-switch-screen=gigya-register-screen\u003eSign up\u003c/a\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003eBy signing in, you agree to our \u003ca class=treg-gya-termsofservice-link\u003eTerms of Service\u003c/a\u003e and \u003ca class=treg-gya-privacy-link\u003ePrivacy Policy\u003c/a\u003e.\u003c/div\u003e\u003c/div\u003e\u003cdiv class="treg-conditional treg-is-premium"\u003e\u003cdiv class="gya-vcenter treg-conditional treg-is-premium treg-gya-onprofile-page"\u003e\u003cp\u003e\u003cb\u003eHave a Chronicle Subscription?\u003cbr\u003eLook up your account to create a log in.\u003c/b\u003e\u003c/div\u003e\u003ca class="treg-conditional treg-is-premium gya-btn treg-gya-subscribe-lookup-link" href=#\u003eLook Up Account\u003c/a\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003eBy signing in, you agree to our \u003ca class=treg-gya-termsofservice-link\u003eTerms of Service\u003c/a\u003e and \u003ca class=treg-gya-privacy-link\u003ePrivacy Policy\u003c/a\u003e.\u003c/div\u003e\u003chr class="gya-rule treg-conditional treg-is-premium"\u003e\u003cdiv class="treg-conditional treg-is-premium gya-vcenter"\u003e\u003cp\u003e\u003cb\u003eDon\u0027t have a subscription?\u003c/b\u003e\u003c/div\u003e\u003ca class="treg-conditional treg-is-premium treg-gya-subscribe-link profile-subscribe-btn" href=#\u003eSubscribe\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-register-screen data-width=auto\u003e\u003cform class=gigya-register-form\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eCreate a profile using\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003c/div\u003e\u003ch6 class=gya-rule\u003e\u003cspan\u003eor\u003c/span\u003e\u003c/h6\u003e\u003cinput class="gya-txt gigya-input-text" style=width:100% name=email formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=email\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003ePasswords must be at least 8 characters, contain upper and lowercase letters and at least one number.\u003c/div\u003e\u003cinput class=gya-txt type=password name=password style=width:100% placeholder=Password\u003e \u003cinput class="gya-txt gigya-input-password" type=password placeholder="Confirm Password" name=passwordRetype tabindex=0\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=password\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=passwordRetype\u003e\u003cdiv class=gya-vcenter\u003e\u003cdiv class=gya-left\u003e\u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e\u003c/div\u003e\u003cdiv class=gya-right style=display:none\u003e\u003cinput type=checkbox value=second_checkbox checked\u003e\u003clabel\u003eSend me email offers\u003c/label\u003e\u003c/div\u003e\u003c/div\u003e\u003cinput class=gya-btn style=width:100% type=submit value="Create a profile"\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003eAlready have a profile? \u003ca data-switch-screen=gigya-login-screen\u003eSign In\u003c/a\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003eBy clicking "Next" or signing in you confirm that you accept our \u003ca href="http://www.sfchronicle.com/terms_of_use/"\u003eTerms and Conditions\u003c/a\u003e and have read and understood our \u003ca href="http://www.sfchronicle.com/privacy_policy/"\u003ePrivacy Policy\u003c/a\u003e.\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-complete-registration-screen data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eComplete Sign Up!\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-section\u003e\u003cp class=gya-txt-left\u003e\u003cstrong\u003eYou\u0027re almost there!\u003c/strong\u003e\u003cp class=gya-txt-left\u003ePlease verify your email in order to complete your profile. Check your email for a profile verification link.\u003cp class=gya-txt-left\u003eYou will not have access to writing comments, managing newsletters or alerts, and updating your profile until you verify your account with the link provided.\u003cp class=gya-txt-left\u003eThe verification link in the email will expire in 24 hours.\u003c/div\u003e\u003cform class=gigya-profile-form data-on-success-screen=gigya-verification-sent-screen\u003e\u003cp class=gya-txt-left\u003eTo resend the verification email, please enter your email address below. The email address submitted will be sent a profile verification link.\u003c/p\u003e\u003cinput style=width:100% name=email class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=email\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-submit" style=display:block\u003e\u003cinput type=submit class="gigya-input-submit gya-btn" value="REQUEST LINK" tabindex=0\u003e\u003c/div\u003e\u003cdiv class="gigya-error-display gigya-composite-control gigya-composite-control-form-error" data-bound-to=gigya-profile-form\u003e\u003cp class="gigya-error-msg gigya-form-error-msg gya-msg" data-bound-to=gigya-profile-form\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-forgot-password-screen data-width=auto\u003e\u003cform class=gigya-reset-password-form data-on-success-screen=gigya-forgot-password-success-screen\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password?\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp class=gya-txt-left\u003eProvide your email address to receive a password reset link.\u003c/div\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=loginID\u003e\u003cdiv class="gigya-error-display gigya-composite-control gigya-composite-control-form-error" data-bound-to=gigya-reset-password-form\u003e\u003cdiv class="gigya-error-msg gigya-form-error-msg" data-bound-to=gigya-reset-password-form\u003e\u003c/div\u003e\u003c/div\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-submit submit-wrapper"\u003e\u003cinput type=submit class="gigya-input-submit gya-btn" value=Submit tabindex=0\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer" data-switch-screen=gigya-login-screen\u003eCancel\u003c/a\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-forgot-password-success-screen data-width=auto\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp class=gya-txt-left\u003eAn email regarding your password change has been sent to your email address.\u003c/div\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer gya-btn" data-switch-screen=gigya-login-screen\u003eReturn to Sign In\u003c/a\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-verification-pending-screen data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eComplete Sign Up!\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-section\u003e\u003cp class=gya-txt-left\u003e\u003cstrong\u003eYou\u0027re almost there!\u003c/strong\u003e\u003cp class=gya-txt-left\u003ePlease verify your email in order to complete your profile. Check your email for a profile verification link.\u003cp class=gya-txt-left\u003eYou will not have access to writing comments, managing newsletters or alerts, and updating your profile until you verify your account with the link provided.\u003cp class=gya-txt-left\u003eThe verification link in the email will expire in 24 hours.\u003c/div\u003e\u003cform class=gigya-resend-verification-code-form data-on-success-screen=gigya-verification-sent-screen\u003e\u003cp class=gya-txt-left\u003eTo resend the verification email, please enter your email address below. The email address submitted will be sent a profile verification link.\u003c/p\u003e\u003cinput name=email class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=email\u003e\u003c/p\u003e\u003cinput type=submit class="gigya-input-submit gya-btn" value="REQUEST LINK" tabindex=0\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-verification-sent-screen data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eComplete Sign Up!\u003c/h5\u003e\u003c/div\u003e\u003cp class=gya-success\u003eA verification email with a link to verify your account has been sent to you.\u003c/p\u003e\u003ca class=gya-btn data-switch-screen=gigya-login-screen\u003eReturn to sign in\u003c/a\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-user-logged-in data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eHi, \u003cspan class=treg-gya-displayname\u003e\u003c/span\u003e\u003c/h5\u003e\u003c/div\u003e\u003ch5 class="gya-vcenter gya-alt-header"\u003eYou are signed in\u003c/h5\u003e\u003cdiv\u003e\u003cdiv class="treg-conditional treg-is-subscriber"\u003e\u003cdiv class=gya-desc\u003e\u003cp class=gya-txt-left\u003eMaintain your display name, email address, password and newsletters in Profile.\u003c/div\u003e\u003ca href=# class="treg-gya-mangage-profile-link gya-btn"\u003eMANAGE PROFILE\u003c/a\u003e\u003cdiv class="gya-vcenter gya-rule"\u003e\u003c/div\u003e\u003cdiv class=gya-desc\u003e\u003cp class=gya-txt-left\u003eMaintain your delivery address, payment details and place a vacation hold in Subscriber Portal.\u003c/div\u003e\u003ca href=# class="treg-gya-manage-subscription-link gya-btn"\u003eMANAGE SUBSCRIPTION\u003c/a\u003e\u003c/div\u003e\u003cdiv class="treg-conditional treg-is-not-subscriber"\u003e\u003cdiv class=treg-conditional\u003e\u003cdiv class=gya-desc\u003e\u003cp class=gya-desc-left\u003eMaintain your display name, email address, password and newsletters in Profile.\u003c/div\u003e\u003ca href=# class="treg-gya-mangage-profile-link gya-btn"\u003eMANAGE PROFILE\u003c/a\u003e\u003c/div\u003e\u003cdiv class="gya-vcenter gya-rule"\u003e\u003c/div\u003e\u003ca href=# class="treg-gya-subscribe-link gya-btn"\u003eSUBSCRIBE\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gya-vcenter gya-rule"\u003e\u003c/div\u003e\u003ca href=javascript:void(0) class="treg-gya-logout gya-btn"\u003eSIGN OUT\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gigya-screen-set id=hearst-link-accounts-screenset data-on-pending-registration-screen=hearst-registration-screen-set/gigya-complete-registration-screen data-on-pending-verification-screen=hearst-registration-screen-set/gigya-email-verification-screen data-on-pending-password-change-screen=hearst-registration-screen-set/gigya-password-change-required-screen data-on-existing-login-identifier-screen=hearst-link-accounts-screenset/gigya-link-account-screen data-on-accounts-linked-screen=hearst-link-accounts-screenset/gigya-link-accountslinked data-start-screen=gigya-link-account-screen\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-link-account-screen data-width=auto\u003e\u003cform class=gigya-link-accounts-form id=gigya-link-accounts-form\u003e\u003cdiv class=gigya-container data-login-identities=site-only\u003e\u003ch5 class=gya-alt-header\u003eThis is the first time you have logged in with a social network.\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gigya-container data-login-identities=social\u003e\u003ch5 class=gya-alt-header\u003eYou have previously logged in with a different social network.\u003c/h5\u003e\u003c/div\u003e\u003cdiv class="gigya-container sub-title-text gya-vcenter" data-login-identities=site-and-social\u003e\u003cp class=gya-txt-left\u003eConnect with an existing social network account:\u003c/div\u003e\u003cdiv class="gigya-container sub-title-text gya-vcenter" data-login-identities=social-only\u003e\u003cp class=gya-txt-left\u003eTo connect with your existing account, click below:\u003c/div\u003e\u003cdiv data-login-identities=social\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-social-login gigya-builder-v2"\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003cparam name=loginMode value=link\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter data-login-identities=site-and-social\u003e\u003cp class=gya-txt-left\u003ePlease provide your password to connect your existing profile:\u003c/div\u003e\u003cdiv data-login-identities=site\u003e\u003cinput style=width:100% name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput type=password name=password style=width:100% placeholder=Password class=gya-txt\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=loginID\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=password\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cdiv class=gya-left\u003e\u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003ca data-switch-screen=gigya-link-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot password?\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter style=clear:both\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eKeep me signed in\u003c/label\u003e\u003c/div\u003e\u003cinput class=gya-btn style=width:100% type=submit value="Sign in"\u003e\u003c/form\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003eBy signing in, you agree to our\u003cbr\u003e \u003ca class=treg-gya-termsofservice-link\u003eTerms of Service\u003c/a\u003e and \u003ca class=treg-gya-privacy-link\u003ePrivacy Policy\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-link-accountslinked data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eAccounts Linked\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-section\u003e\u003cp class=gya-txt-left\u003eYou have linked your accounts, tap below to log in:\u003c/div\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-link-forgot-password-screen data-width=auto\u003e\u003cform class=gigya-reset-password-form data-on-success-screen=gigya-link-forgot-password-success-screen\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password?\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp class=gya-txt-left\u003eProvide your email address to receive a password reset link.\u003c/div\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=loginID\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-submit submit-wrapper"\u003e\u003cinput type=submit class="gigya-input-submit gya-btn" value=Submit tabindex=0\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer" data-switch-screen=gigya-link-account-screen\u003eCancel\u003c/a\u003e\u003c/div\u003e\u003cdiv class="gigya-error-display gigya-composite-control gigya-composite-control-form-error" data-bound-to=gigya-reset-password-form\u003e\u003cdiv class="gigya-error-msg gigya-form-error-msg" data-bound-to=gigya-reset-password-form\u003e\u003c/div\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-link-forgot-password-success-screen data-width=auto\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp class=gya-txt-left\u003eAn email regarding your password change has been sent to your email address.\u003c/div\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer gya-btn" data-switch-screen=gigya-link-account-screen\u003eReturn to Sign In\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen-set gya-container" id=hearst-premium-article-set style=display:none\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-premium-login data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eSign In\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-not-mobile\u003e\u003cform class=gigya-login-form\u003e\u003cdiv class=gya-vcenter\u003e\u003cdiv class=gya-input-wrap\u003e\u003cinput class=gya-btn type=submit value="SIGN IN"\u003e\u003cdiv class=gya-left\u003e\u003cinput class=gya-txt name=loginID formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eKeep me signed in\u003c/label\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003cinput class=gya-txt type=password name=password placeholder=Password\u003e \u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e \u003ca data-switch-screen=gigya-premium-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot password?\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-login-form data-error-codes=403042 data-scope=bound-object-error\u003eEmail Address or Password is incorrect.\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003ePassword field is required.\u003c/form\u003e\u003c/div\u003e\u003cdiv class=gya-mobile\u003e\u003cform class=gigya-login-form\u003e\u003cinput class=gya-txt style=width:100% name=loginID formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput class=gya-txt type=password name=password style=width:100% placeholder=Password\u003e\u003cdiv class=gya-vcenter\u003e\u003cdiv class=gya-left\u003e\u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003ca data-switch-screen=gigya-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot password?\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-login-form data-error-codes=403042 data-scope=bound-object-error\u003eEmail Address or Password is incorrect.\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003ePassword field is required.\u003cdiv class=gya-vcenter style=clear:both\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eKeep me signed in\u003c/label\u003e\u003c/div\u003e\u003cinput class=gya-btn style=width:100% type=submit value="Sign in"\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gya-vcenter gya-footer"\u003e\u003cp\u003eBy signing in, you agree to our \u003ca class=treg-gya-termsofservice-link\u003eTerms of Service\u003c/a\u003e and \u003ca class=treg-gya-privacy-link\u003ePrivacy Policy\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-premium-user-logged-in data-width=auto\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-premium-user-logged-in-limbo data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003ePaid Subscription Access Only\u003c/h5\u003e\u003c/div\u003e\u003cp class=gya-txt-left\u003eYou are now signed in but you must have a San Francisco Chronicle subscription to access any content.\u003cp class=gya-txt-left\u003eAlready have a subscription?\u003c/p\u003e\u003ca href=# class="treg-gya-activate-subscription-link gya-btn"\u003eACTIVATE\u003c/a\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-premium-forgot-password-screen data-width=auto\u003e\u003cform class=gigya-reset-password-form data-on-success-screen=gigya-premium-forgot-password-success-screen\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password?\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp class=gya-txt-left\u003eProvide your email address to receive a password reset link.\u003c/div\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=loginID\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-submit submit-wrapper"\u003e\u003cinput type=submit class="gigya-input-submit gya-btn" value=Submit tabindex=0\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer" data-switch-screen=gigya-premium-login\u003eCancel\u003c/a\u003e\u003c/div\u003e\u003cdiv class="gigya-error-display gigya-composite-control gigya-composite-control-form-error" data-bound-to=gigya-reset-password-form\u003e\u003cdiv class="gigya-error-msg gigya-form-error-msg" data-bound-to=gigya-reset-password-form\u003e\u003c/div\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-premium-forgot-password-success-screen data-width=auto\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp class=gya-txt-left\u003eAn email regarding your password change has been sent to your email address.\u003c/div\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer gya-btn" data-switch-screen=gigya-premium-login\u003eReturn to Sign In\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen-set gya-widget control-widget" id=hearst-profile-screen-set style=display:none\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-edit-profile data-width=auto\u003e\u003cform class=gigya-profile-form\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eAbout Me\u003c/h5\u003e\u003cdiv class=gya-edit-btn\u003e\u003cdiv class=treg-edit-profile-button\u003e\u003cinput class="gya-btn gya-btn-small" type=button name=EDIT value=EDIT onclick="return treg.gya.edit_profile()"\u003e\u003c/div\u003e\u003cdiv class=treg-cancel-edit-profile-button style=display:none\u003e\u003cinput class="gya-btn gya-btn-small" type=button name=CANCEL value=CANCEL onclick="treg.gya.cancel_edit_profile();return false"\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-left\u003e\u003cp class="gigya-label-text gya-label gya-req"\u003eDISPLAY NAME\u003cspan class=gya-info\u003e\u003c/span\u003e \u003cspan class=gya-info-text\u003eDisplay names must begin with an alpha-numeric character. They can not contain the following: ! # $ % ^ \u0026 * ( ) | \\ / ? {} [ ] nor may they contain obscenities.\u003c/span\u003e\u003c/p\u003e\u003cinput class="gigya-input-text treg-profile-disabled gya-txt" placeholder="enter display name" name=profile.nickname disabled\u003e\u003cp class="gya-hide gya-msg gya-msg-nickname-in-use"\u003eThis display name is already in use. Please enter a different display name.\u003cp class="gya-hide gya-msg gya-msg-nickname-is-empty"\u003eYou must enter a display name.\u003cp class="gya-hide gya-msg gya-msg-nickname-complexity"\u003eDisplay names must begin with an alpha-numeric character. They can not contain the following: ! # $ % ^ \u0026 * ( ) | \\ / ? {} [ ] nor may they contain obscenities.\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003cp class="gigya-label-text gya-label gya-req"\u003eEMAIL ADDRESS\u003cspan class=gya-info\u003e\u003c/span\u003e \u003cspan class=gya-info-text\u003eChanging this email address will also change the email address associated with your subscription\u003c/span\u003e\u003c/p\u003e\u003cinput name=profile.email maxlength=80 placeholder="enter email address" class="gigya-input-text treg-profile-disabled gya-txt" formnovalidate tabindex=0 disabled\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=profile.email\u003e\u003cp class="gya-hide gya-msg gya-msg-email-is-empty"\u003eYou must enter an email address.\u003c/div\u003e\u003cdiv class=gya-left\u003e\u003cp class="gigya-label-text gya-label"\u003eGENDER\u003cspan class=gya-info\u003e\u003c/span\u003e \u003cspan class=gya-info-text\u003eThis optional field is required to enter contests and promotions\u003c/span\u003e\u003c/p\u003e\u003cselect name=profile.gender data-display-name="" class=treg-profile-disabled disabled\u003e\u003coption value=""\u003eNot Specified\u003coption value=m\u003eMale\u003coption value=f\u003eFemale\u003coption value=u\u003ePrefer not to answer\u003c/select\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=profile.gender\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003cp class="gigya-label-text gya-label"\u003eBIRTH YEAR\u003cspan class=gya-info\u003e\u003c/span\u003e \u003cspan class=gya-info-text\u003eThis optional field is required to enter contests and promotions\u003c/span\u003e\u003c/p\u003e\u003cselect name=profile.birthYear tabindex=0 class=treg-profile-disabled disabled\u003e\u003coption value=""\u003eNot Specified\u003coption value=1920\u003e1920\u003coption value=1921\u003e1921\u003coption value=1922\u003e1922\u003coption value=1923\u003e1923\u003coption value=1924\u003e1924\u003coption value=1925\u003e1925\u003coption value=1926\u003e1926\u003coption value=1927\u003e1927\u003coption value=1928\u003e1928\u003coption value=1929\u003e1929\u003coption value=1930\u003e1930\u003coption value=1931\u003e1931\u003coption value=1932\u003e1932\u003coption value=1933\u003e1933\u003coption value=1934\u003e1934\u003coption value=1935\u003e1935\u003coption value=1936\u003e1936\u003coption value=1937\u003e1937\u003coption value=1938\u003e1938\u003coption value=1939\u003e1939\u003coption value=1940\u003e1940\u003coption value=1941\u003e1941\u003coption value=1942\u003e1942\u003coption value=1943\u003e1943\u003coption value=1944\u003e1944\u003coption value=1945\u003e1945\u003coption value=1946\u003e1946\u003coption value=1947\u003e1947\u003coption value=1948\u003e1948\u003coption value=1949\u003e1949\u003coption value=1950\u003e1950\u003coption value=1951\u003e1951\u003coption value=1952\u003e1952\u003coption value=1953\u003e1953\u003coption value=1954\u003e1954\u003coption value=1955\u003e1955\u003coption value=1956\u003e1956\u003coption value=1957\u003e1957\u003coption value=1958\u003e1958\u003coption value=1959\u003e1959\u003coption value=1960\u003e1960 \u003coption value=1961\u003e1961 \u003coption value=1962\u003e1962 \u003coption value=1963\u003e1963 \u003coption value=1964\u003e1964 \u003coption value=1965\u003e1965 \u003coption value=1966\u003e1966 \u003coption value=1967\u003e1967 \u003coption value=1968\u003e1968 \u003coption value=1969\u003e1969 \u003coption value=1970\u003e1970 \u003coption value=1971\u003e1971 \u003coption value=1972\u003e1972 \u003coption value=1973\u003e1973 \u003coption value=1974\u003e1974 \u003coption value=1975\u003e1975 \u003coption value=1976\u003e1976 \u003coption value=1977\u003e1977 \u003coption value=1978\u003e1978 \u003coption value=1979\u003e1979 \u003coption value=1980\u003e1980 \u003coption value=1981\u003e1981 \u003coption value=1982\u003e1982 \u003coption value=1983\u003e1983 \u003coption value=1984\u003e1984 \u003coption value=1985\u003e1985 \u003coption value=1986\u003e1986 \u003coption value=1987\u003e1987 \u003coption value=1988\u003e1988 \u003coption value=1989\u003e1989 \u003coption value=1990\u003e1990 \u003coption value=1991\u003e1991 \u003coption value=1992\u003e1992 \u003coption value=1993\u003e1993 \u003coption value=1994\u003e1994 \u003coption value=1995\u003e1995 \u003coption value=1996\u003e1996 \u003coption value=1997\u003e1997 \u003coption value=1998\u003e1998 \u003coption value=1999\u003e1999 \u003coption value=2000\u003e2000 \u003coption value=2001\u003e2001 \u003coption value=2002\u003e2002 \u003c/select\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=profile.birthYear\u003e\u003c/div\u003e\u003cdiv class="gya-req-txt gya-left"\u003e\u003cp class=gya-txt-left\u003e* are required fields\u003c/div\u003e\u003cdiv class=treg-save-profile-button style=display:none\u003e\u003cinput type=submit class="gigya-input-submit gya-btn" value="SAVE PROFILE" tabindex=0\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen-set gya-container control-widget" id=hearst-manage-subscription-screen-set\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-manage-subscription data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003e\u003cspan class=treg-gya-premium-site-name-abbrev\u003e\u003c/span\u003e Subscription\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=treg-is-subscriber\u003e\u003cdiv class=gya-section\u003e\u003cp class=gya-txt-left\u003eYou can manage your delivery address, payment details, delete your account and more subscription details in the subscriber portal.\u003c/div\u003e\u003ca href=# class="treg-gya-manage-subscription-link gya-btn"\u003eManage Subscription\u003c/a\u003e\u003c/div\u003e\u003cdiv class=treg-is-not-subscriber\u003e\u003cdiv class=gya-subscribe\u003e\u003cdiv\u003e\u003ch6\u003eGet it on all your devices\u003c/h6\u003e\u003ch2\u003eUnlimited digital access with every subscription\u003c/h2\u003e\u003cp class=gya-txt-left\u003eSFChronicle.com | App for iPhone and iPad\u003cbr\u003ee-edition (daily print replica) | Exclusive member experiences and discounts\u003c/p\u003e\u003ca href=# class="treg-gya-subscribe-link gya-btn"\u003eSUBSCRIBE NOW AND SAVE!\u003c/a\u003e \u003ca href=# class="treg-gya-activate-subscription-link gya-btn gya-btn-alt"\u003eI have a subscription\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen-set gya-container control-widget" id=hearst-manage-password-screen-set style=display:none\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-password-home data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003ePassword\u003c/h5\u003e\u003cdiv class=gya-edit-btn\u003e\u003ca data-switch-screen=gigya-change-password-screen class="gya-btn gya-btn-small"\u003eEDIT\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-section\u003e\u003cp class=gya-txt-left\u003eThis password applies to SFGate and San Francisco Chronicle including the San Francisco Chronicle subscription and account management page.\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=gigya-change-password-screen data-width=auto\u003e\u003cform class=gigya-profile-form data-on-success-screen=gigya-password-home\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003ePassword\u003c/h5\u003e\u003cdiv class=gya-edit-btn\u003e\u003ca data-switch-screen=gigya-password-home class="gya-btn gya-btn-small"\u003eCANCEL\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-row\u003e\u003cdiv class="gya-section gya-left"\u003e\u003cp class=gya-txt-left\u003eThis password applies to SFGate and San Francisco Chronicle including the San Francisco Chronicle subscription and account management page.\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003cp class="gigya-label-text gya-label"\u003eCURRENT PASSWORD \u003cspan class=gya-info\u003e\u003c/span\u003e \u003cspan class=gya-info-text\u003ePasswords must be at least 8 characters and contain both upper and lowercase letters and at least one number.\u003c/span\u003e\u003c/p\u003e\u003cinput type=password name=password class="gigya-input-password gya-txt" tabindex=0 placeholder="Current Password" autocomplete=off\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-profile-form data-error-codes=403042 data-scope=bound-object-error\u003eCurrent password is incorrect\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003eCurrent password field is required\u003cp class="gigya-error-msg gya-msg" data-bound-to=password data-error-codes=400009\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-row\u003e\u003cdiv class=gya-left\u003e\u003cp class="gigya-label-text gya-label"\u003eNEW PASSWORD \u003cspan class=gya-info\u003e\u003c/span\u003e \u003cspan class=gya-info-text\u003ePasswords must be at least 8 characters and contain both upper and lowercase letters and at least one number.\u003c/span\u003e\u003c/p\u003e\u003cinput type=password name=newPassword class="gigya-input-password gya-txt" placeholder="New Password" tabindex=0\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=newPassword data-error-codes=400027 data-scope=bound-object-error\u003eNew password field is required\u003cp class="gigya-error-msg gya-msg" data-bound-to=newPassword data-error-codes=400009\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003cp class="gigya-label-text gya-label"\u003eCONFIRM PASSWORD \u003cspan class=gya-info\u003e\u003c/span\u003e \u003cspan class=gya-info-text\u003ePasswords must be at least 8 characters and contain both upper and lowercase letters and at least one number.\u003c/span\u003e\u003c/p\u003e\u003cinput type=password name=passwordRetype class="gigya-input-password gya-txt" placeholder="Confirm Password" tabindex=0\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=passwordRetype data-error-codes=400027 data-scope=bound-object-error\u003eConfirm password field is required\u003cp class="gigya-error-msg gya-msg" data-bound-to=passwordRetype data-error-codes=400009\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-row\u003e\u003cdiv class="gya-left gya-vcenter"\u003e\u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e\u003c/div\u003e\u003cdiv class="gya-right gya-vcenter"\u003e\u003cinput type=submit class="gigya-input-submit gya-btn gya-btn-small" value="SAVE PASSWORD" tabindex=0\u003e\u003c/div\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen-set gya-container control-widget" id=hearst-profile-widget-screen-set style=display:none\u003e\u003cdiv class="gigya-screen gya-profile" id=gigya-profile-user-logged-in data-width=auto\u003e\u003cdiv class="gya-greeting gya-widget"\u003e\u003ch2\u003eWelcome, \u003cspan class=treg-gya-displayname\u003e\u003c/span\u003e!\u003c/h2\u003e\u003cp class=gya-txt-left\u003eYour profile, subscription, linked accounts, and newsletter selections are not viewable by other readers on our sites. Your profile can only be seen by you. Any comments you post to articles on our sites can be seen by all readers and moderators.\u003c/div\u003e\u003cdiv class="gya-left-col gya-profile"\u003e\u003cdiv class=control-widget id=treg-edit-profile-widget\u003e\u003c/div\u003e\u003cdiv class="control-widget treg-conditional treg-has-site-login" id=treg-password-widget\u003e\u003c/div\u003e\u003cdiv class="gya-newsletters gya-widget"\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eSign Up for Newsletters!\u003c/h5\u003e\u003ca href=# class="treg-gya-stnewsletter-link gya-btn-small gya-btn"\u003eSIGN UP\u003c/a\u003e\u003c/div\u003e\u003cp\u003eSign up for and manage your preferences for all the email newsletters and marketing from the San Francisco Chronicle and SFGATE teams.\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gigya-screen id=gigya-profile-user-login data-width=auto\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-hide id=hearst-commenting-bar-widget-screen-set\u003e\u003cdiv id=admin-bar class=comments-bar\u003e\u003cp class="signedout treg-conditional treg-not-logged-in"\u003eYou must be signed in to comment\u003cp class="signedin treg-conditional treg-logged-in"\u003e\u003cspan class=treg-gya-displayname\u003e\u003c/span\u003e is currently signed in\u003c/p\u003e\u003ca href=javascript:void(0) id=treg_captureSignInLink class="comments-toggle signInLink"\u003eSign In\u003c/a\u003e \u003ca href=javascript:void(0) class="comments-toggle user signOutLink"\u003e\u003cspan class=icon\u003e \u003c/span\u003e\u003c/a\u003e\u003c/div\u003e\u003cdiv class=gya-toggle style=display:none\u003e\u003cdiv class=treg-gya-login-widget\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen-set gya-container" id=syncronex-overlay-set style=display:none\u003e\u003cdiv class="gigya-screen gya-widget gya-nag-widget" id=syncronex-overlay-login data-width=auto\u003e\u003cdiv class=gya-not-mobile\u003e\u003cform class=gigya-login-form\u003e\u003cdiv class=gya-left\u003e\u003cdiv class="gya-vcenter gya-clip"\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-login-form data-error-codes=403042 data-scope=bound-object-error\u003eEmail Address or Password is incorrect.\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003ePassword field is required.\u003cp class="treg-conditional treg-not-logged-in"\u003eIf you are a subscriber, Sign in!\u003cp class="treg-conditional treg-is-not-subscriber"\u003eYou must be a subscriber to access this content.\u003c/div\u003e\u003cinput class=gya-txt name=loginID formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput class=gya-txt type=password name=password placeholder=Password\u003e \u003cinput class=gya-btn type=submit value="Sign in"\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eStay signed in\u003c/label\u003e \u003ca data-switch-screen=syncronex-overlay-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot Password?\u003c/a\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class=gya-mobile\u003e\u003cform class=gigya-login-form\u003e\u003cdiv class="gya-vcenter gya-clip"\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-login-form data-error-codes=403042 data-scope=bound-object-error\u003eEmail Address or Password is incorrect.\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003ePassword field is required.\u003cp class="treg-conditional treg-not-logged-in"\u003eIf you are a subscriber, Sign in!\u003cp class="treg-conditional treg-is-not-subscriber"\u003eYou must be a subscriber to access this content.\u003c/div\u003e\u003cinput class=gya-txt name=loginID formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput class=gya-txt type=password name=password placeholder=Password\u003e\u003cdiv class="gya-vcenter gya-left"\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eStay signed in\u003c/label\u003e\u003c/div\u003e\u003cdiv class="gya-vcenter gya-right"\u003e\u003ca data-switch-screen=syncronex-overlay-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot Password?\u003c/a\u003e\u003c/div\u003e\u003cinput class=gya-btn type=submit value="Sign in"\u003e\u003c/form\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget gya-nag-widget" id=syncronex-overlay-forgot-password-screen data-width=auto\u003e\u003cdiv class=gya-not-mobile\u003e\u003cform class=gigya-reset-password-form data-on-success-screen=syncronex-overlay-forgot-password-success-screen\u003e\u003cdiv class=gya-left\u003e\u003cdiv class="gya-vcenter gya-clip"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=loginID\u003e\u003cp class="gigya-error-msg gigya-form-error-msg gya-msg" data-bound-to=gigya-reset-password-form\u003e\u003cp\u003eProvide your email address to receive a password reset link.\u003c/div\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput type=submit class="gigya-input-submit gya-btn" value=Submit tabindex=0\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer" data-switch-screen=syncronex-overlay-login\u003eCancel\u003c/a\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class=gya-mobile\u003e\u003cform class=gigya-reset-password-form data-on-success-screen=syncronex-overlay-forgot-password-success-screen\u003e\u003cdiv class="gya-vcenter gya-clip"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=loginID\u003e\u003cp class="gigya-error-msg gigya-form-error-msg gya-msg" data-bound-to=gigya-reset-password-form\u003e\u003cp\u003eProvide your email address to receive a password reset link.\u003c/div\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput type=submit class="gigya-input-submit gya-btn" value=Submit tabindex=0\u003e\u003cdiv class=gya-vcenter\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer" data-switch-screen=syncronex-overlay-login\u003eCancel\u003c/a\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget gya-nag-widget" id=syncronex-overlay-forgot-password-success-screen data-width=auto\u003e\u003cp class=gya-txt-left\u003eAn email regarding your password change has been sent to your email address.\u003c/p\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer gya-btn" data-switch-screen=syncronex-overlay-login\u003eReturn to Sign In\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen-set gya-container" id=syncronex-offers-set style=display:none\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-offers-login data-width=auto data-on-existing-login-identifier-screen=syncronex-link-accounts-screenset2/syncronex-link-account-screen2\u003e\u003cdiv class=gya-not-mobile\u003e\u003cform class=gigya-login-form\u003e\u003cdiv class="gya-vcenter gya-clip"\u003e\u003cdiv class=gya-left\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-login-form data-error-codes=403042 data-scope=bound-object-error\u003eEmail Address or Password is incorrect.\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003ePassword field is required.\u003cp class="treg-conditional treg-is-not-subscriber"\u003eYou must be a subscriber to access this content.\u003cp class="treg-conditional treg-not-logged-in"\u003eIf you are a subscriber, Sign in!\u003c/div\u003e\u003cdiv class="gya-right gya-txt-right"\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eStay signed in\u003c/label\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-input-wrap\u003e\u003cdiv class="gya-left gya-social"\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=142\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gya-right gya-offers-form"\u003e\u003cinput class=gya-txt name=loginID formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput class=gya-txt type=password name=password placeholder=Password\u003e \u003cinput class=gya-btn type=submit value="Sign in"\u003e\u003cdiv class="gya-input-wrap gya-txt-right"\u003e\u003ca data-switch-screen=syncronex-offers-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot Your Password?\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class=gya-mobile\u003e\u003cform class=gigya-login-form\u003e\u003cdiv class="gya header gya-vcenter"\u003e\u003ch5\u003eSign In\u003c/h5\u003e\u003c/div\u003e\u003cdiv class="gya-vcenter gya-clip"\u003e\u003cp class="treg-conditional treg-not-logged-in"\u003eIf you are a subscriber, Sign in!\u003cp class="gya-msg treg-conditional treg-is-not-subscriber"\u003eYou must be a subscriber to access this content.\u003c/div\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003c/div\u003e\u003ch6 class=gya-rule\u003e\u003cspan\u003eor\u003c/span\u003e\u003c/h6\u003e\u003cinput class=gya-txt name=loginID formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput class=gya-txt type=password name=password placeholder=Password\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-login-form data-error-codes=403042 data-scope=bound-object-error\u003eEmail Address or Password is incorrect.\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003ePassword field is required.\u003cdiv class="gya-vcenter gya-left"\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eStay signed in\u003c/label\u003e\u003c/div\u003e\u003cdiv class="gya-vcenter gya-right"\u003e\u003ca data-switch-screen=syncronex-offers-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot Password?\u003c/a\u003e\u003c/div\u003e\u003cinput class=gya-btn type=submit value=Submit\u003e\u003c/form\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-offers-forgot-password-screen data-width=auto\u003e\u003cform class=gigya-reset-password-form data-on-success-screen=syncronex-offers-forgot-password-success-screen\u003e\u003cp\u003eProvide your email address to receive a password reset link.\u003c/p\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput type=submit class="gigya-input-submit gya-btn" value=Submit tabindex=0\u003e\u003cp class=gya-not-mobile\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer" data-switch-screen=syncronex-offers-login\u003eCancel\u003c/a\u003e\u003cp class=gya-mobile\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer" data-switch-screen=syncronex-offers-login\u003eBack to Sign In\u003c/a\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-reset-password-form\u003eEmail address is incorrect.\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-offers-forgot-password-success-screen data-width=auto\u003e\u003cp class=gya-txt-left\u003eAn email regarding your password change has been sent to your email address.\u003c/p\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer gya-btn" data-switch-screen=syncronex-offers-login\u003eReturn to Sign In\u003c/a\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-offers-user-logged-in-limbo data-width=auto\u003e\u003cp class="gya-msg treg-conditional treg-is-not-subscriber"\u003eYou must be a subscriber to access this content.\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gigya-screen-set id=syncronex-link-accounts-screenset data-on-existing-login-identifier-screen=syncronex-link-accounts-screenset/syncronex-link-account-screen data-on-accounts-linked-screen=syncronex-link-accounts-screenset/syncronex-link-accountslinked data-start-screen=syncronex-link-account-screen\u003e\u003cdiv class="gigya-screen gya-widget gya-link-accounts" id=syncronex-link-account-screen data-width=auto\u003e\u003cform class=gigya-link-accounts-form\u003e\u003cdiv class=gya-input-wrap\u003e\u003ch5\u003eLink Profiles\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gigya-container data-login-identities=site-only\u003e\u003cp class=gya-txt-left\u003eYou must link your profile in order to continue. Please provide your password or select a social network to connect your existing profile with your social network.\u003c/div\u003e\u003cdiv class=gigya-container data-login-identities=social-only\u003e\u003cp class=gya-txt-left\u003eYou must link your profile in order to continue. Please provide your password or select a social network to connect your existing profile with your social network.\u003c/div\u003e\u003cdiv class="gigya-container sub-title-text" data-login-identities=site-and-social\u003e\u003cp class=gya-txt-left\u003eYou must link your profile in order to continue. Please provide your password or select a social network to connect your existing profile with your social network.\u003c/div\u003e\u003cdiv data-login-identities=social\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-social-login gigya-builder-v2"\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003cparam name=loginMode value=link\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv data-login-identities=site\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput type=password name=password placeholder=Password class=gya-txt\u003e\u003c/div\u003e\u003cdiv class="gya-input-wrap gya-checkbox-wrap"\u003e\u003cdiv class=gya-left\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eKeep me signed in\u003c/label\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gya-vcenter gya-forgot-pass"\u003e\u003ca data-switch-screen=syncronex-link-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot Your Password?\u003c/a\u003e\u003c/div\u003e\u003cdiv class=gya-input-wrap\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-login-form data-error-codes=403042 data-scope=bound-object-error\u003eEmail Address or Password is incorrect.\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003ePassword field is required.\u003c/div\u003e\u003cinput class=gya-btn type=submit value="Sign in"\u003e\u003cdiv class="gya-right gya-forgot-pass"\u003e\u003ca data-switch-screen=syncronex-link-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot Your Password?\u003c/a\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-link-accountslinked data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eAccounts Linked\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-section\u003e\u003cp class=gya-txt-left\u003eYou have linked your accounts, tap below to log in:\u003c/div\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-link-forgot-password-screen data-width=auto\u003e\u003cform class=gigya-reset-password-form data-on-success-screen=syncronex-link-forgot-password-success-screen\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password?\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp class=gya-txt-left\u003eProvide your email address to receive a password reset link.\u003c/div\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=loginID\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-submit submit-wrapper"\u003e\u003cinput type=submit class="gigya-input-submit gya-btn" value=Submit tabindex=0\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer" data-switch-screen=syncronex-link-account-screen\u003eCancel\u003c/a\u003e\u003c/div\u003e\u003cdiv class="gigya-error-display gigya-composite-control gigya-composite-control-form-error" data-bound-to=gigya-reset-password-form\u003e\u003cdiv class="gigya-error-msg gigya-form-error-msg" data-bound-to=gigya-reset-password-form\u003e\u003c/div\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-link-forgot-password-success-screen data-width=auto\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password\u003c/h5\u003e\u003c/div\u003e\u003cp class=gya-txt-left\u003eAn email regarding your password change has been sent to your email address.\u003c/p\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer gya-btn" data-switch-screen=syncronex-link-account-screen\u003eReturn to Sign In\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gigya-screen-set id=syncronex-link-accounts-screenset2 data-on-existing-login-identifier-screen=syncronex-link-accounts-screenset2/syncronex-link-account-screen2 data-on-accounts-linked-screen=syncronex-link-accounts-screenset2/syncronex-link-accountslinked2 data-start-screen=syncronex-link-account-screen2\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-link-account-screen2 data-width=auto\u003e\u003cdiv class=gya-not-mobile\u003e\u003cform class=gigya-link-accounts-form\u003e\u003cdiv class="gya-vcenter gya-clip"\u003e\u003cdiv class=gya-left\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-login-form data-error-codes=403042 data-scope=bound-object-error\u003eEmail Address or Password is incorrect.\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003ePassword field is required.\u003cp class="treg-conditional treg-not-logged-in"\u003eLink Profiles\u003c/div\u003e\u003cdiv class="gya-right gya-txt-right"\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eStay signed in\u003c/label\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-container sub-title-text" data-login-identities=site-only\u003e\u003cp class=gya-txt-left\u003eYou must link your profile in order to continue. Please provide your password or select a social network to connect your existing profile with your social network.\u003c/div\u003e\u003cdiv class="gigya-container sub-title-text" data-login-identities=social-only\u003e\u003cp class=gya-txt-left\u003eYou must link your profile in order to continue. Please provide your password or select a social network to connect your existing profile with your social network.\u003c/div\u003e\u003cdiv class="gigya-container sub-title-text" data-login-identities=site-and-social\u003e\u003cp class=gya-txt-left\u003eYou must link your profile in order to continue. Please provide your password or select a social network to connect your existing profile with your social network.\u003c/div\u003e\u003cdiv class="gya-left gya-social"\u003e\u003cdiv data-login-identities=social\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-social-login gigya-builder-v2"\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=142\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003cparam name=loginMode value=link\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gya-right gya-offers-form"\u003e\u003cdiv data-login-identities=site\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput type=password name=password placeholder=Password class=gya-txt\u003e \u003cinput class=gya-btn type=submit value="Sign in"\u003e\u003c/div\u003e\u003cdiv class="gya-input-wrap gya-txt-right"\u003e\u003ca data-switch-screen=syncronex-offers-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot Your Password?\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class=gya-mobile\u003e\u003cdiv class=gya-vcenter\u003e\u003ch5\u003eSIGN IN\u003c/h5\u003e\u003c/div\u003e\u003cform class=gigya-link-accounts-form\u003e\u003cdiv class=gigya-container data-login-identities=site-only\u003e\u003ch5 class=gya-alt-header\u003eThis is the first time you have logged in with a social network.\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gigya-container data-login-identities=social\u003e\u003ch5 class=gya-alt-header\u003eYou have previously logged in with a different social network.\u003c/h5\u003e\u003c/div\u003e\u003cdiv class="gigya-container sub-title-text gya-vcenter" data-login-identities=site-and-social\u003e\u003cp class=gya-txt-left\u003eConnect with an existing social network account:\u003c/div\u003e\u003cdiv class="gigya-container sub-title-text gya-vcenter" data-login-identities=social-only\u003e\u003cp class=gya-txt-left\u003eTo connect with your existing account, click below:\u003c/div\u003e\u003cdiv data-login-identities=social\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-social-login gigya-builder-v2"\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003cparam name=loginMode value=link\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter data-login-identities=site-and-social\u003e\u003cp class=gya-txt-left\u003ePlease provide your password to connect your existing profile:\u003c/div\u003e\u003cdiv data-login-identities=site\u003e\u003cinput style=width:100% name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput type=password name=password style=width:100% placeholder=Password class=gya-txt\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=loginID\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=password\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cdiv class=gya-left\u003e\u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003ca data-switch-screen=syncronex-link-forgot-password-screen2 class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot Your Password?\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter style=clear:both\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eKeep me signed in\u003c/label\u003e\u003c/div\u003e\u003cinput class=gya-btn style=width:100% type=submit value="Sign in"\u003e\u003c/form\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-link-accountslinked2 data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eAccounts Linked\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-section\u003e\u003cp class=gya-txt-left\u003eYou have linked your accounts, tap below to log in:\u003c/div\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-link-forgot-password-screen2 data-width=auto\u003e\u003cform class=gigya-reset-password-form data-on-success-screen=syncronex-link-forgot-password-success-screen2\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password?\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp class=gya-txt-left\u003eProvide your email address to receive a password reset link.\u003c/div\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=loginID\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-submit submit-wrapper"\u003e\u003cinput type=submit class="gigya-input-submit gya-btn" value=Submit tabindex=0\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer" data-switch-screen=syncronex-link-account-screen2\u003eCancel\u003c/a\u003e\u003c/div\u003e\u003cdiv class="gigya-error-display gigya-composite-control gigya-composite-control-form-error" data-bound-to=gigya-reset-password-form\u003e\u003cdiv class="gigya-error-msg gigya-form-error-msg" data-bound-to=gigya-reset-password-form\u003e\u003c/div\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-link-forgot-password-success-screen2 data-width=auto\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password\u003c/h5\u003e\u003c/div\u003e\u003cp class=gya-txt-left\u003eAn email regarding your password change has been sent to your email address.\u003c/p\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer gya-btn" data-switch-screen=syncronex-link-account-screen2\u003eReturn to Sign In\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen-set gya-container" id=syncronex-main-login-set style=display:none\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-main-login data-width=auto data-on-existing-login-identifier-screen=syncronex-link-accounts-screenset/syncronex-link-account-screen\u003e\u003cform class=gigya-login-form\u003e\u003cdiv class="treg-conditional treg-synx-nonmanagement-page"\u003eSign In With Your Existing \u003cspan class=treg-gya-premium-site-name\u003e\u003c/span\u003e Account\u003c/div\u003e\u003cdiv class="treg-conditional treg-synx-management-page"\u003eSign In\u003c/div\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003c/div\u003e\u003cdiv class=section-divider\u003e\u003cspan\u003eOR\u003c/span\u003e\u003c/div\u003e\u003cinput class=gya-txt style=width:40% name=loginID formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput class=gya-txt type=password name=password style=width:40% placeholder=Password\u003e\u003cdiv\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-login-form data-error-codes=403042 data-scope=bound-object-error\u003eEmail Address or Password is incorrect.\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003ePassword field is required.\u003c/div\u003e\u003cdiv class=gya-input-wrap\u003e\u003cdiv class=gya-left style=width:50%\u003e\u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e\u003c/div\u003e\u003cdiv class=gya-right style=width:50%\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eKeep me signed in\u003c/label\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-input-wrap\u003e\u003cdiv class=gya-left style=width:50%\u003e\u003ca data-switch-screen=syncronex-main-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot Your Password?\u003c/a\u003e\u003c/div\u003e\u003cdiv class=gya-right style=width:50%\u003e\u003cinput class=gya-btn style=width:20% type=submit value="Sign in"\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gya-input-wrap gya-tnc"\u003e\u003cp style=text-align:center\u003eBy signing in, you agree to our \u003ca class=treg-gya-termsofservice-link\u003eterms of use\u003c/a\u003e.\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-main-forgot-password-screen data-width=auto\u003e\u003cform class=gigya-reset-password-form data-on-success-screen=syncronex-main-forgot-password-success-screen\u003e\u003ch5\u003eReset your password?\u003c/h5\u003e\u003cp\u003eProvide your email address to receive a password reset link.\u003c/p\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput type=submit class="gigya-input-submit gya-btn" value=Submit tabindex=0\u003e\u003cp\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer" data-switch-screen=syncronex-main-login\u003eCancel\u003c/a\u003e\u003cdiv class=gya-vcenter\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=loginID\u003e\u003cdiv class="gigya-error-display gigya-composite-control gigya-composite-control-form-error" data-bound-to=gigya-reset-password-form\u003e\u003cdiv class="gigya-error-msg gigya-form-error-msg" data-bound-to=gigya-reset-password-form\u003e\u003c/div\u003e\u003c/div\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-main-forgot-password-success-screen data-width=auto\u003e\u003ch5\u003eReset your password?\u003c/h5\u003e\u003cp class=gya-txt-left\u003eAn email regarding your password change has been sent to your email address.\u003c/p\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer gya-btn" data-switch-screen=syncronex-main-login\u003eReturn to Sign In\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen-set gya-container" id=syncronex-registration-screen-set style=display:none data-on-pending-registration-screen=syncronex-registration-screen-set/syncronex-complete-registration-screen data-on-pending-verification-screen=syncronex-registration-screen-set/syncronex-verification-pending-screen data-on-pending-password-change-screen=syncronex-registration-screen-set/syncronex-password-change-required-screen data-on-existing-login-identifier-screen=syncronex-link-accounts-screenset/syncronex-link-account-screen\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-login-screen data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eSign in using your existing information\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003c/div\u003e\u003ch6 class=gya-rule\u003e\u003cspan\u003eor\u003c/span\u003e\u003c/h6\u003e\u003cform class=gigya-login-form\u003e\u003cinput class=gya-txt style=width:100% name=loginID formnovalidate tabindex=0 placeholder="Email address"\u003e \u003cinput class=gya-txt type=password name=password style=width:100% placeholder=Password\u003e\u003cp class="gigya-error-display gya-msg" data-bound-to=gigya-login-form data-error-codes=403042 data-scope=bound-object-error\u003eEmail Address or Password is incorrect.\u003cp class="gigya-error-display gya-msg" data-bound-to=loginID data-error-codes=400027 data-scope=bound-object-error\u003eEmail address field is required.\u003cp class="gigya-error-display gya-msg" data-bound-to=password data-error-codes=400027 data-scope=bound-object-error\u003ePassword field is required.\u003cdiv class=not-mobile\u003e\u003cdiv class=gya-vcenter\u003e\u003cdiv class=gya-left\u003e\u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003ca data-switch-screen=syncronex-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot password?\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter style=clear:both\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eKeep me signed in\u003c/label\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=is-mobile\u003e\u003cdiv class=gya-vcenter\u003e\u003cdiv class=gya-left\u003e\u003cinput type=checkbox name=remember class=gigya-input-checkbox tabindex=0 checked\u003e \u003clabel class=gigya-label\u003eKeep me signed in\u003c/label\u003e\u003c/div\u003e\u003cdiv class=gya-right\u003e\u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e\u003c/div\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003ca data-switch-screen=syncronex-forgot-password-screen class="gigya-forgotPassword gigya-composite-control gigya-composite-control-link"\u003eForgot password?\u003c/a\u003e\u003c/div\u003e\u003c/div\u003e\u003cinput class=gya-btn style=width:100% type=submit value="Sign in"\u003e\u003c/form\u003e\u003ca class="treg-conditional treg-is-free gya-btn" data-switch-screen=syncronex-register-screen\u003eCreate an account\u003c/a\u003e\u003cdiv class=gya-vcenter\u003e\u003cp class=gya-tnc\u003eBy signing in, you agree to our \u003ca class=treg-gya-termsofservice-link\u003eTerms of Service\u003c/a\u003e and \u003ca class=treg-gya-privacy-link\u003ePrivacy Policy\u003c/a\u003e.\u003c/div\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-register-screen data-width=auto\u003e\u003cform class=gigya-register-form\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eCreate a sign-in for your subscription. This sign-in allows you to use our digital products and services.\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gigya-social-login\u003e\u003cparam name=buttonsStyle value=icons\u003e\u003cparam name=width value=265\u003e\u003cparam name=height value=44\u003e\u003cparam name=buttonSize value=44\u003e\u003cparam name=showTermsLink value=false\u003e\u003cparam name=hideGigyaLink value=true\u003e\u003cparam name=enabledProviders value=facebook,google,linkedin\u003e\u003c/div\u003e\u003ch6 class=gya-rule\u003e\u003cspan\u003eor\u003c/span\u003e\u003c/h6\u003e\u003cinput class="gya-txt gigya-input-text" style=width:100% name=email formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=email\u003e\u003c/p\u003e\u003cinput class=gya-txt type=password name=password style=width:100% placeholder=Password\u003e \u003cinput class="gya-txt gigya-input-password" type=password placeholder="Confirm Password" name=passwordRetype tabindex=0\u003e\u003cdiv class=gya-info-wrap\u003e\u003cspan class=gya-info\u003e\u003c/span\u003e \u003cspan class=gya-info-text\u003e\u003cspan class=gya-notch\u003e\u003c/span\u003ePasswords must be at least 8 characters, contain upper and lowercase letters and at least one number.\u003c/span\u003e\u003c/div\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=password\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=passwordRetype\u003e\u003cdiv class=gya-vcenter\u003e\u003cdiv class=gya-left\u003e\u003cinput type=checkbox class=treg-gya-showpwdcb\u003e\u003clabel\u003eShow password\u003c/label\u003e\u003c/div\u003e\u003cdiv class=gya-right style=display:none\u003e\u003c/div\u003e\u003c/div\u003e\u003cinput class=gya-btn style=width:100% type=submit value=Next\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003eAlready have a sign-in for SFGATE or San Francisco Chronicle?\u003c/p\u003e\u003ca data-switch-screen=syncronex-login-screen\u003eSign In\u003c/a\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003eContact Customer Service at 800-310-2455 or email us at \u003ca href=mailto:homedelivery@sfchronicle.com\u003ehomedelivery@sfchronicle.com\u003c/a\u003e.\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-complete-registration-screen data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eComplete Sign Up!\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-section\u003e\u003cp class=gya-txt-left\u003e\u003cstrong\u003eYou\u0027re almost there!\u003c/strong\u003e\u003cp class=gya-txt-left\u003ePlease verify your email in order to complete your profile. Check your email for a profile verification link.\u003cp class=gya-txt-left\u003eYou will not have access to writing comments, managing newsletters or alerts, and updating your profile until you verify your account with the link provided.\u003cp class=gya-txt-left\u003eThe verification link in the email will expire in 24 hours.\u003c/div\u003e\u003cform class=gigya-profile-form data-on-success-screen=syncronex-verification-sent-screen\u003e\u003cp class=gya-txt-left\u003eTo resend the verification email, please enter your email address below. The email address submitted will be sent a profile verification link.\u003c/p\u003e\u003cinput style=width:100% name=email class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=email\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-submit" style=display:block\u003e\u003cinput type=submit class="gigya-input-submit gya-btn" value="REQUEST LINK" tabindex=0\u003e\u003c/div\u003e\u003cdiv class="gigya-error-display gigya-composite-control gigya-composite-control-form-error" data-bound-to=gigya-profile-form\u003e\u003cp class="gigya-error-msg gigya-form-error-msg gya-msg" data-bound-to=gigya-profile-form\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-forgot-password-screen data-width=auto\u003e\u003cform class=gigya-reset-password-form data-on-success-screen=syncronex-forgot-password-success-screen\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password?\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp class=gya-txt-left\u003eProvide your email address to receive a password reset link.\u003c/div\u003e\u003cinput name=loginID class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=loginID\u003e\u003cdiv class="gigya-error-display gigya-composite-control gigya-composite-control-form-error" data-bound-to=gigya-reset-password-form\u003e\u003cp class="gigya-error-msg gigya-form-error-msg gya-msg" data-bound-to=gigya-reset-password-form\u003e\u003c/div\u003e\u003cdiv class="gigya-composite-control gigya-composite-control-submit submit-wrapper"\u003e\u003cinput type=submit class="gigya-input-submit gya-btn" value=Submit tabindex=0\u003e\u003c/div\u003e\u003cdiv class=gya-vcenter\u003e\u003cp\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer" data-switch-screen=syncronex-login-screen\u003eCancel\u003c/a\u003e\u003c/div\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-forgot-password-success-screen data-width=auto\u003e\u003cdiv class="gya-vcenter gya-header"\u003e\u003ch5\u003eReset your password\u003c/h5\u003e\u003c/div\u003e\u003cp class=gya-txt-left\u003eAn email regarding your password change has been sent to your email address.\u003c/p\u003e\u003ca class="gigya-composite-control gigya-composite-control-link gigya-footer gya-btn" data-switch-screen=syncronex-login-screen\u003eReturn to Sign In\u003c/a\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-verification-pending-screen data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eComplete Sign Up!\u003c/h5\u003e\u003c/div\u003e\u003cdiv class=gya-section\u003e\u003cp class=gya-txt-left\u003e\u003cstrong\u003eYou\u0027re almost there!\u003c/strong\u003e\u003cp class=gya-txt-left\u003ePlease verify your email in order to complete your profile. Check your email for a profile verification link.\u003cp class=gya-txt-left\u003eYou will not have access to writing comments, managing newsletters or alerts, and updating your profile until you verify your account with the link provided.\u003cp class=gya-txt-left\u003eThe verification link in the email will expire in 24 hours.\u003c/div\u003e\u003cform class=gigya-resend-verification-code-form data-on-success-screen=syncronex-verification-sent-screen\u003e\u003cp class=gya-txt-left\u003eTo resend the verification email, please enter your email address below. The email address submitted will be sent a profile verification link.\u003c/p\u003e\u003cinput name=email class="gigya-input-text gya-txt" formnovalidate tabindex=0 placeholder="Email address"\u003e\u003cp class="gigya-error-msg gya-msg" data-bound-to=email\u003e\u003c/p\u003e\u003cinput type=submit class="gigya-input-submit gya-btn" value="REQUEST LINK" tabindex=0\u003e\u003c/form\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-verification-sent-screen data-width=auto\u003e\u003cdiv class="gya-header gya-vcenter"\u003e\u003ch5\u003eComplete Sign Up!\u003c/h5\u003e\u003c/div\u003e\u003cp class=gya-success\u003eA verification email with a link to verify your account has been sent to you.\u003c/p\u003e\u003ca class=gya-btn data-switch-screen=syncronex-login-screen\u003eReturn to sign in\u003c/a\u003e\u003c/div\u003e\u003cdiv class="gigya-screen gya-widget" id=syncronex-user-logged-in data-width=auto\u003e\u003c/div\u003e\u003c/div\u003e';
treg.gya.siteNewsLetters={"st10":{id:"st10",extId:"53e012a5027083684d8b4567",name:"Top O’ the Bay",type:"free"},"st20":{id:"st20",extId:"53e0130ba89eec1a448b4568",name:"Notes & Errata (Mark Morford)",type:"free"},"st30":{id:"st30",extId:"53e01298a89eec1d448b4567",name:"Breaking News",type:"free"},"st40":{id:"st40",extId:"53e012cc027083794d8b4568",name:"Inside Scoop",type:"free"},"st50":{id:"st50",extId:"53e0131ba89eec3f448b4567",name:"New Flicks",type:"free"},"st60":{id:"st60",extId:"53e012f80270836c4d8b4567",name:"BizTek",type:"free"},"st70":{id:"st70",extId:"5501fe423b35d041438b4567",name:"5 Things to Do",type:"free"},"st80":{id:"st80",extId:"5744ec6c3c8aa93d598b456f",name:"Sports",type:"free"},"st90":{id:"st90",extId:"5457d2f85f1d5b432adaab86",name:"Surveys",type:"free"},"st100":{id:"st100",extId:"5457d2a11092038d7ce3112d",name:"Sales and Promotions",type:"free"},"st110":{id:"st110",extId:"5457d31b5f1d5b5f1ddaab88",name:"3rd Party/Advertising",type:"free"},"st120":{id:"st120",extId:"5457d069109203427ce3112b",name:"Chronicle Commuter",type:"paid"},"st140":{id:"st140",extId:"5457d245109203b604e3112b",name:"Membership Updates",type:"paid"},"st150":{id:"st150",extId:"5457d21e5f1d5b162bdaab85",name:"e-edition Reminder",type:"paid"},"st160":{id:"st160",extId:"5457d2d65f1d5b671ddaab87",name:"Subscription Reminders",type:"paid"},"st170":{id:"st170",extId:"5457d2675f1d5b0409daab8a",name:"General Content Promotions",type:"paid"}};
treg.gya.hasBadWords=function(b){var c=b.toLowerCase();
for(var a=0;
a<treg.gya.badwordlist.length;
a++){if(c.indexOf(treg.gya.badwordlist[a])!=-1){return true
}}return false
};
treg.gya.maskBadWords=function(e){var b="X";
var f=e.toLowerCase();
for(var a=0;
a<treg.gya.badwordlist.length;
a++){var c=new Array(treg.gya.badwordlist[a].length+1).join(b);
var d=new RegExp("\\b"+treg.gya.badwordlist[a]+"\\b","gi");
e=e.replace(d,c)
}return e
};
treg.gya.badwordlist=["2g1c","2 girls 1 cup","acrotomophilia","anal","anilingus","anus","arsehole","ass","asshole","assmunch","auto erotic","autoerotic","babeland","baby batter","ball gag","ball gravy","ball kicking","ball licking","ball sack","ball sucking","bangbros","bareback","barely legal","barenaked","bastardo","bastinado","bbw","bdsm","beaver cleaver","beaver lips","bestiality","bi curious","big black","big breasts","big knockers","big tits","bimbos","birdlock","bitch","black cock","blonde action","blonde on blonde action","blow j","blow your l","blue waffle","blumpkin","bollocks","bondage","boner","boob","boobs","booty call","brown showers","brunette action","bukkake","bulldyke","bullet vibe","bung hole","bunghole","busty","butt","buttcheeks","butthole","camel toe","camgirl","camslut","camwhore","carpet muncher","carpetmuncher","chocolate rosebuds","circlejerk","cleveland steamer","clit","clitoris","clover clamps","clusterfuck","cock","cocks","coprolagnia","coprophilia","cornhole","cum","cumming","cunnilingus","cunt","darkie","date rape","daterape","deep throat","deepthroat","dick","dildo","dirty pillows","dirty sanchez","dog style","doggie style","doggiestyle","doggy style","doggystyle","dolcett","domination","dominatrix","dommes","donkey punch","double dong","double penetration","dp action","eat my ass","ecchi","ejaculation","erotic","erotism","escort","ethical slut","eunuch","faggot","fecal","felch","fellatio","feltch","female squirting","femdom","figging","fingering","fisting","foot fetish","footjob","frotting","fuck","fuck buttons","fudge packer","fudgepacker","futanari","g-spot","gang bang","gay sex","genitals","giant cock","girl on","girl on top","girls gone wild","goatcx","goatse","gokkun","golden shower","goo girl","goodpoop","goregasm","grope","group sex","guro","hand job","handjob","hard core","hardcore","hentai","homoerotic","honkey","hooker","hot chick","how to kill","how to murder","huge fat","humping","incest","intercourse","jack off","jail bait","jailbait","jerk off","jigaboo","jiggaboo","jiggerboo","jizz","juggs","kike","kinbaku","kinkster","kinky","knobbing","leather restraint","leather straight jacket","lemon party","lolita","lovemaking","make me come","male squirting","masturbate","menage a trois","milf","missionary position","motherfucker","mound of venus","mr hands","muff diver","muffdiving","nambla","nawashi","negro","neonazi","nig nog","nigga","nigger","nimphomania","nipple","nipples","nsfw images","nude","nudity","nympho","nymphomania","octopussy","omorashi","one cup two girls","one guy one jar","orgasm","orgy","paedophile","panties","panty","pedobear","pedophile","pegging","penis","phone sex","piece of shit","piss pig","pissing","pisspig","playboy","pleasure chest","pole smoker","ponyplay","poof","poop chute","poopchute","porn","porno","pornography","prince albert piercing","pthc","pubes","pussy","queef","raghead","raging boner","rape","raping","rapist","rectum","reverse cowgirl","rimjob","rimming","rosy palm","rosy palm and her 5 sisters","rusty trombone","s&m","sadism","scat","schlong","scissoring","semen","sex","sexo","sexy","shaved beaver","shaved pussy","shemale","shibari","shit","shota","shrimping","slanteye","slut","smut","snatch","snowballing","sodomize","sodomy","spic","spooge","spread legs","strap on","strapon","strappado","strip club","style doggy","suck","sucks","suicide girls","sultry women","swastika","swinger","tainted love","taste my","tea bagging","threesome","throating","tied up","tight white","tit","tits","titties","titty","tongue in a","topless","tosser","towelhead","tranny","tribadism","tub girl","tubgirl","tushy","twat","twink","twinkie","two girls one cup","undressing","upskirt","urethra play","urophilia","vagina","venus mound","vibrator","violet wand","vorarephilia","voyeur","vulva","wank","wet dream","wetback","white power","women rapping","wrapping men","wrinkled starfish","xx","xxx","yaoi","yellow showers","yiffy","zoophilia"];
treg.ssoId="gigya";
treg.logInfo("main treg gigya script loading");
treg.isPremium=treg.isPremium||false;
treg.isMobile=(document.location.hostname.toLowerCase().indexOf("m.sfgate.com")!=-1);
treg.gya.hasScreensets=false;
treg.subscribeLink="/subscribe/";
treg.subscribeLookupLink="treg.subscribe.lookup.link";
treg.manageSubscriptionLink="/account/";
treg.activateSubscriptionLink="https://syncaccess-hst-sfc.syncronex.com/portal/#/activation?return="+encodeURIComponent(window.location.href);
treg.profileLink=treg.isMobile?"http://m.sfgate.com/profile/":"http://www.sfgate.com/profile/";
treg.privacyLink="http://www.sfgate.com/privacy-policy/";
treg.termsLink="http://www.sfgate.com/termsandconditions/";
treg.sailthruNewsletterLink=null;
treg.rebuild=true;
treg.isRedirecting=false;
treg.suppressLoggedInScreens=false;
treg.gya.redirect_url=null;
treg.domLoaded=function(){try{if(treg.ssoId=="gigya"){$(".treg-not-gigya").hide();
$(".treg-is-gigya").show()
}else{$(".treg-is-gigya").hide();
$(".treg-not-gigya").show()
}}catch(a){treg.logInfo("exception trying to show or hide sso specifics.")
}treg.logInfo("domLoaded");
treg.domLoaded=true;
treg.fireEvent(treg.event.on_dom_loaded)
};
treg.contentLoaded(window,treg.domLoaded);
treg.gya.loadGigya=function(){window.__gigyaConf={gigyaPlugins:{"sessionExpiration":0,"rememberSessionExpiration":-2}};
var a="3_BMFZOCwXDrSMoeTYN2uiVIBZhHski0qbm2oj7S3NL2gKLHcAjq-sx3cuKnLVg_pN";
if(treg.isMobile){a="3_r_R0r8xFNnvD4ViteU4gfcMWtSZUct60v_ZIz9RAS2-tvjS6R0uRq3ktsORPavqZ"
}var c=document.createElement("script");
var f="https:"==document.location.protocol;
var e=f?"cdns":"cdn";
var b="&cbust=20180221";
var d=treg.getFullExpandedAddress(e+".gigya.com/js/gigya.js?apiKey="+a+b);
c.type="text/javascript";
c.async=true;
c.src=d;
c.text=getGigyaLoaderText();
document.getElementsByTagName("head")[0].appendChild(c);
treg.logInfo("loading api key: "+a)
};
function getGigyaLoaderText(){var a='{siteName: "sfgate.com", sessionExpiration: -2';
var b=navigator.userAgent.indexOf("Safari")>-1;
if((b)&&(navigator.userAgent.indexOf("Chrome")>-1||navigator.userAgent.indexOf("Android")>-1)){b=false
}if(b){treg.logInfo("SSO Token enabled for unsupported platform.");
a+=", enableSSOToken: true}"
}else{a+="}"
}return a
}treg.gya.loadGigya();
function onGigyaServiceReady(a){treg.logInfo("onGigyaServiceReady");
treg.unqueueCommands();
$(document).ready(treg.initializeGigya)
}if(!treg.developerMode){treg.loadCSS(treg.getFullExpandedAddress("treg.hearstnp.com/assets/gya_sites/sfgate_com/css/79ae6edb11dccb44c3c365891a96680ab1f702dc.css"))
}$(document).ready(function(){if(!treg.developerMode||treg.localCSS){treg.logInfo("Appending treg screensets to page");
var a=$("<div />").appendTo("body");
a.hide();
a.html(treg.html["gigya-screensets.html"])
}});
treg.gya.accountInfo=null;
treg.gya.federated=false;
treg.gya.automaticlogin=true;
treg.gya.loaded=false;
treg.gya.activeSession=false;
treg.gya.hasProfileEditor=false;
treg.gya.jsonPCall=function(b,a){b=b+"&callback="+a;
treg.loadScriptAsync(b)
};
treg.endSession=function(){gigya.accounts.logout()
};
treg.gya.getAccountInfoResponse=function(b){if(treg.gya.loaded==false&&b.errorCode==0){treg.gya.automaticlogin=true
}treg.gya.accountInfo=b;
if(b.errorCode==0){treg.logInfo("found user session");
treg.gya.activeSession=true;
treg.gya.updateIdentity();
treg.logInfo("updated identity");
if(treg.identity.displayName==""||typeof(treg.identity.displayName)=="undefined"){treg.logInfo("getting a new display name");
var a=treg.gya.generateSafeDisplayName();
treg.gya.EnforceNameUniqueness(a,"treg.gya.EnforceNameUniquenessCallback");
return
}else{treg.fireEvent(treg.event.onSessionFound)
}}else{treg.logInfo("user session not found");
treg.gya.activeSession=false;
treg.fireEvent(treg.event.onSessionNotFound)
}treg.logInfo("gya loaded state: "+treg.gya.loaded);
if(treg.gya.loaded==false){treg.fireEvent(treg.event.onInitialSessionState)
}treg.gya.loaded=true
};
treg.gya.EnforceNameUniqueness=function(c,a){var b="https://edb.sfchronicle.com/WCMPaywall/EnsureUniqueName?name=";
b+=encodeURIComponent(c);
treg.gya.jsonPCall(b,a)
};
treg.gya.EnforceNameUniquenessCallback=function(a){var b={profile:{nickname:a},callback:function(c){if(c.errorCode!=0){treg.logInfo("user data could not be updated.")
}else{treg.logInfo("user data updated.");
treg.identity.displayName=a;
treg.fireEvent(treg.event.onSessionFound);
treg.fireEvent(treg.event.onInitialSessionState);
treg.gya.loaded=true
}}};
treg.fireEvent(treg.event.onSessionFound,b);
gigya.accounts.setAccountInfo(b)
};
treg.gya.onLogin=function(a){if(treg.gya.loaded&&treg.gya.activeSession){treg.gya.federated=true;
treg.logInfo("federated login")
}else{gigya.accounts.getAccountInfo({callback:treg.gya.getAccountInfoResponse})
}};
treg.gya.onLogout=function(a){treg.logInfo("onLogout");
treg.clearIdentity();
treg.gya.activeSession=false;
treg.fireEvent(treg.event.onSessionEnd)
};
treg.initializeGigya=function(){if(!treg.gigyaServiceAvailable){treg.logInfo("gigya service is currently down. Treg will not be loaded.");
treg.gya.showServiceUnavailable();
return
}treg.logInfo("initializeGigya");
gigya.accounts.addEventHandlers({onLogin:treg.gya.onLogin,onLogout:treg.gya.onLogout});
gigya.accounts.getAccountInfo({callback:treg.gya.getAccountInfoResponse})
};
treg.gya.showServiceUnavailable=function(){var a=$("<div />").appendTo("body");
a.hide();
a.html(treg.html["gigya-screensets.html"]);
$("#hearst-gigya-unavailable .treg-gya-supportdesk-phone").html("(415) 777-1111");
$("#hearst-gigya-unavailable .treg-gya-supportdesk-email").html("help@sfgate.com");
var b=$("#hearst-gigya-unavailable").html();
$(".treg-gya-profile-widget").each(function(){$(this).html(b)
});
$(".treg-gya-login-widget").each(function(){$(this).html(b)
});
$(".treg-gya-premiumarticle-login-widget").each(function(){$(this).html(b)
})
};
treg.hasActiveSession=function(){return(treg.identity.id!=null)
};
treg.isUserPremiumSubscriber=function(){try{return(treg.identity.edbId!=null)
}catch(a){return false
}};
treg.clearIdentity=function(){treg.identity.id=null;
treg.identity.janrainId=null;
treg.identity.edbId=null;
treg.identity.displayName=null;
treg.identity.email=null;
treg.identity.loginDate=null;
treg.identity.socialNetwork=null;
treg.identity.automaticLogin=true;
treg.identity.pageType=treg.pageType;
treg.identity.pageMode="content";
treg.identity.lastLoginMethod="";
treg.identity.currentLoginMethod="";
try{treg.identity.lastLoginMethod=treg.identity.currentLoginMethod
}catch(a){}treg.identity.currentLoginMethod=null
};
treg.gya.updateIdentity=function(){treg.clearIdentity();
if(!treg.gya.activeSession){return
}var a=treg.gya.accountInfo;
if(typeof(treg.subscriberid)!="undefined"){treg.identity.edbId=treg.subscriberid
}else{if(typeof(a.data.hearstEdbid0)!="undefined"){treg.identity.edbId=a.data.hearstEdbid0
}}treg.identity.janrainId=a.data.janrainID;
treg.identity.id=a.UID;
treg.identity.displayName=a.profile.nickname;
treg.identity.email=a.profile.email;
treg.identity.loginDate=new Date();
treg.identity.automaticLogin=treg.gya.automaticlogin;
treg.identity.socialNetwork="";
treg.logInfo("Setting treg identity "+treg.identity.email);
treg.identity.currentLoginMethod=a.loginProvider=="site"?"traditionalSignin":"socialSignin";
if(a.loginProvider!=="site"){treg.identity.socialNetwork=a.loginProvider
}};
treg.gya.passwordCheckBoxes=[];
treg.gya.hookupShowPwd=function(a){treg.gya.passwordCheckBoxes.push(a);
$("#"+a+" .treg-gya-showpwdcb").change(function(){var c=this;
if(typeof(c.passwordboxes)=="undefined"){c.passwordboxes=[];
$("input:password").each(function(){if(c.form==this.form){c.passwordboxes.push(this)
}})
}for(var b=0;
b<c.passwordboxes.length;
b++){this.passwordboxes[b].type=this.checked?"text":"password"
}})
};
treg.gya.revertShowPasswordCheckboxes=function(b){for(var a=0;
a<treg.gya.passwordCheckBoxes.length;
a++){$("#"+treg.gya.passwordCheckBoxes[a]+" .treg-gya-showpwdcb").each(function(c,d){if(typeof b!="undefined"){if(b.find(d).length==0){return
}}if(d.checked){d.checked=false;
$(d).change()
}})
}};
treg.gya.initialize_login_widgets=function(){var b=this;
var a={screenset:"hearst-registration-screen-set",loginScreen:"gigya-login-screen",loggedInScreen:"gigya-user-logged-in",registerScreen:"gigya-register-screen"};
if(typeof(b._initialized)=="undefined"){b.login_widgets=[];
b._initialized=true;
treg.gya.on_loginScreenLoaded=function(c){if(c.currentScreen==a.loggedInScreen){}else{if(c.currentScreen==a.loginScreen||c.currentScreen=="gigya-register-screen"||c.currentScreen=="gigya-link-account-screen"){treg.gya.hookupShowPwd(c.sourceContainerID)
}}treg.fireEvent(treg.event.onScreenRendered,c)
};
treg.registerEvent(treg.event.onSessionFound,function(){if(treg.suppressLoggedInScreens){return
}for(var d=0;
d<b.login_widgets.length;
d++){var c=b.login_widgets[d].id;
gigya.accounts.showScreenSet({screenSet:a.screenset,containerID:c,startScreen:a.loggedInScreen,onAfterScreenLoad:treg.gya.on_loginScreenLoaded})
}});
treg.registerEvent(treg.event.onSessionEnd,function(){for(var d=0;
d<b.login_widgets.length;
d++){var c=b.login_widgets[d].id;
gigya.accounts.showScreenSet({screenSet:a.screenset,containerID:c,startScreen:a.loginScreen,onAfterScreenLoad:treg.gya.on_loginScreenLoaded})
}})
}treg.logInfo("Found "+$(".treg-gya-login-widget").length+" .treg-gya-login-widget on page");
$(".treg-gya-login-widget").each(function(d,g){var c=this.id;
var e=$(this).hasClass("treg-inside-screenset");
var f=$(this).hasClass("treg-login-instanced");
if(!e&&!f){c="treg-gya-login-widget"+b.login_widgets.length;
b.login_widgets.push(this);
this.id=c;
$(this).addClass("treg-login-instanced");
treg.gya.hookupShowPwd(c);
if(treg.gya.activeSession&&!treg.suppressLoggedInScreens){gigya.accounts.showScreenSet({screenSet:a.screenset,containerID:c,startScreen:a.loggedInScreen,onAfterScreenLoad:treg.gya.on_loginScreenLoaded})
}else{if(!treg.gya.activeSession){gigya.accounts.showScreenSet({screenSet:a.screenset,containerID:c,startScreen:a.loginScreen,onAfterScreenLoad:treg.gya.on_loginScreenLoaded})
}}treg.logInfo("inititalized login widget: "+c)
}})
};
treg.gya.initialize_premium_login_widgets=function(){var b=this;
var a={screenset:"hearst-premium-article-set",loginScreen:"gigya-premium-login",loggedInScreen:"gigya-premium-user-logged-in",limboScreen:"gigya-premium-user-logged-in-limbo"};
treg.gya.on_ploginScreenLoaded=function(c){if(c.currentScreen==a.loginScreen){treg.gya.hookupShowPwd(c.sourceContainerID)
}};
$(".treg-gya-premiumarticle-login-widget").each(function(d,e){var c=this.id;
if(treg.hasActiveSession()){gigya.accounts.showScreenSet({screenSet:a.screenset,containerID:c,startScreen:a.limboScreen})
}else{gigya.accounts.showScreenSet({screenSet:a.screenset,containerID:c,startScreen:a.loginScreen,onAfterScreenLoad:treg.gya.on_ploginScreenLoaded})
}treg.logInfo("inititalized premium login widget: "+c)
});
treg.registerEvent(treg.event.onSessionFound,function(){var c=(treg.identity.edbId!=null&&treg.identity.edbId!=0);
$(".treg-gya-premiumarticle-login-widget").each(function(f,g){var d=this.id;
var e=(treg.identity.edbId!=null&&treg.identity.edbId!=0);
if(!e){gigya.accounts.showScreenSet({screenSet:a.screenset,containerID:d,startScreen:a.limboScreen})
}else{gigya.accounts.showScreenSet({screenSet:a.screenset,containerID:d,startScreen:a.loggedInScreen})
}})
})
};
treg.gya.initialize_profile_widget=function(){var c=this;
var a=$(".treg-gya-profile-widget").first();
if(a.length==0){treg.logInfo("no profile widgets detected on page");
return
}treg.pageType="profile";
treg.gya.hasProfileEditor=true;
treg.logInfo("instancing profile widget");
var b={screenset:"hearst-profile-widget-screen-set",loginScreen:"gigya-profile-user-login",loggedInScreen:"gigya-profile-user-logged-in",containerId:"gigya-profile-editor-2016",};
a[0].id=b.containerId;
treg.gya.on_prof_screenLoaded=function(d){if(d.currentScreen==b.loggedInScreen){treg.gya.initialize_profile_editor();
treg.logInfo("Activating profile editor")
}else{if(d.currentScreen==b.loginScreen){$("#"+b.containerId+" .treg-gya-login-widget").removeClass("treg-inside-screenset");
treg.gya.initialize_login_widgets()
}}treg.fireEvent(treg.event.onScreenRendered,d)
};
c.onUserLoggedIn=function(){gigya.accounts.showScreenSet({screenSet:b.screenset,containerID:b.containerId,startScreen:b.loggedInScreen,onAfterScreenLoad:treg.gya.on_prof_screenLoaded})
};
c.onUserLoggedOut=function(){gigya.accounts.showScreenSet({screenSet:b.screenset,containerID:b.containerId,startScreen:b.loginScreen,onAfterScreenLoad:treg.gya.on_prof_screenLoaded})
};
treg.registerEvent(treg.event.onSessionFound,function(){treg.fireEvent(treg.event.onBeforeProfileWidget);
if(treg.isRedirecting){return
}c.onUserLoggedIn()
});
treg.registerEvent(treg.event.onSessionEnd,function(){c.onUserLoggedOut()
});
if(treg.gya.activeSession){c.onUserLoggedIn()
}else{c.onUserLoggedOut()
}};
treg.gya.ValidateDisplayNameIsUnique=function(c,a){var b="https://edb.sfchronicle.com/WCMPaywall/IsUniqueName?name=";
b+=encodeURIComponent(c);
treg.gya.jsonPCall(b,a)
};
treg.gya.initialize_profile_editor=function(){var a=this;
treg.gya.email_subscriptions={};
a.init_email_subscriptions=function(){$(".treg-gigya-newslettercb").each(function(b,c){var d=treg.gya.email_subscriptions[c.value];
if(typeof(d)=="undefined"){c.checked=false
}else{c.checked=d.subscribed
}});
$(".treg-gigya-newslettercb").change(function(){var b=$(this).val();
var c=this.checked;
a.updateNewsletterSubscription(b,c)
})
};
a.updateNewsletterSubscription=function(c,d){var e=treg.gya.email_subscriptions[c];
if(typeof(e)=="undefined"){var b=treg.gya.siteNewsLetters[c];
if(typeof(b)=="undefined"){return false
}e={id:c,subscribed:d,updated_utc:0,updated_ext_utc:0};
treg.gya.email_subscriptions[c]=e
}else{e.subscribed=d
}e.updated_utc=new Date().getTime();
gigya.accounts.setAccountInfo({data:{email_subscriptions:JSON.stringify(treg.gya.email_subscriptions),email_subscriptions_updated0:e.updated_utc},callback:function(f){if(f.errorCode!=0){}else{}}});
return true
};
treg.gya.edit_profile=function(){$(".treg-profile-disabled").prop("disabled",false);
$(".treg-edit-profile-button").hide();
$(".treg-cancel-edit-profile-button").show();
$(".treg-save-profile-button").show()
};
treg.gya.cancel_edit_profile=function(){a.setDisplayNameValidity(true);
$(".treg-profile-disabled").prop("disabled",true);
$(".treg-edit-profile-button").show();
$(".treg-cancel-edit-profile-button").hide();
$(".treg-save-profile-button").hide();
$(".gya-hide").hide();
gigya.accounts.showScreenSet({screenSet:"hearst-profile-screen-set",containerID:"treg-edit-profile-widget",startScreen:"gigya-edit-profile",onBeforeSubmit:a.onBeforeSubmit,onAfterSubmit:a.on_profile_updated,onFieldChanged:a.onFieldChanged})
};
a.displayNameValid=true;
a.setDisplayNameValidity=function(b){a.displayNameValid=b;
if(b){$("input[name='profile.nickname']").removeClass("gigya-error");
$("input[name='profile.nickname']").addClass("gigya-valid")
}else{$("input[name='profile.nickname']").removeClass("gigya-valid");
$("input[name='profile.nickname']").addClass("gigya-error")
}};
a.emailValid=true;
a.setEmailValidity=function(b){a.emailValid=b;
if(b){$("input[name='email']").removeClass("gigya-error");
$("input[name='email']").addClass("gigya-valid")
}else{$("input[name='email']").removeClass("gigya-valid");
$("input[name='email']").addClass("gigya-error")
}};
treg.gya.DisplayNameUniqueCallback=function(b){if(b){$(".gya-msg-nickname-in-use").hide();
a.setDisplayNameValidity(true)
}else{$(".gya-msg-nickname-in-use").show();
a.setDisplayNameValidity(false)
}};
a.displayNameChanged=false;
a.validateDisplayName=function(b){var c=/^[A-Za-z0-9_][a-zA-Z0-9.\-_@\s]{2,39}$/;
if(b==""){$(".gya-msg-nickname-is-empty").show();
a.setDisplayNameValidity(false);
return
}$(".gya-msg-nickname-is-empty").hide();
if(!c.test(b)){$(".gya-msg-nickname-complexity").show();
a.setDisplayNameValidity(false);
return
}if(treg.gya.hasBadWords(b)){$(".gya-msg-nickname-complexity").show();
a.setDisplayNameValidity(false);
return
}$(".gya-msg-nickname-complexity").hide();
if(treg.identity.displayName==b){$(".gya-msg-nickname-in-use").hide();
a.setDisplayNameValidity(true);
return
}treg.gya.ValidateDisplayNameIsUnique(b,"treg.gya.DisplayNameUniqueCallback")
};
a.onFieldChanged=function(b){if(b.field=="profile.nickname"){a.displayNameChanged=true;
a.displayNameValid=false;
a.validateDisplayName(b.value)
}else{if(b.field=="profile.email"){if(b.value==""){a.setEmailValidity(false);
$(".gya-msg-email-is-empty").show()
}else{$(".gya-msg-email-is-empty").hide();
a.emailValid=true
}}}};
a.onBeforeSubmit=function(b){return(a.emailValid&&a.displayNameValid)
};
a.on_profile_updated=function(b){if(b.response.errorCode==0){$(".treg-profile-disabled").prop("disabled",true);
$(".treg-edit-profile-button").show();
$(".treg-cancel-edit-profile-button").hide();
$(".treg-save-profile-button").hide();
gigya.accounts.showScreenSet({screenSet:"hearst-profile-screen-set",containerID:"treg-edit-profile-widget",startScreen:"gigya-edit-profile",onBeforeSubmit:a.onBeforeSubmit,onAfterSubmit:a.on_profile_updated,onFieldChanged:a.onFieldChanged});
treg.fireEvent(treg.event.onScreenRendered,b)
}};
gigya.accounts.showScreenSet({screenSet:"hearst-profile-screen-set",containerID:"treg-edit-profile-widget",startScreen:"gigya-edit-profile",onBeforeSubmit:a.onBeforeSubmit,onAfterSubmit:a.on_profile_updated,onFieldChanged:a.onFieldChanged,onAfterScreenLoad:function(b){treg.gya.initCustomMessaging()
}});
gigya.accounts.showScreenSet({screenSet:"hearst-manage-subscription-screen-set",containerID:"treg-manage-subscription-widget",startScreen:"gigya-manage-subscription",});
gigya.accounts.showScreenSet({screenSet:"hearst-manage-password-screen-set",containerID:"treg-password-widget",startScreen:"gigya-password-home",onAfterScreenLoad:function(b){treg.gya.hookupShowPwd(b.sourceContainerID);
treg.gya.initCustomMessaging()
}});
gigya.accounts.getAccountInfo({include:"data",callback:function(c){if(c.errorCode==0){try{treg.gya.email_subscriptions=window.JSON.parse(c.data.email_subscriptions)
}catch(b){}a.init_email_subscriptions()
}}})
};
treg.gya.generateSafeDisplayName=function(){var a="newuser";
if(typeof(treg.identity.email)=="string"){var b=treg.identity.email.split("@")[0];
b=treg.gya.maskBadWords(b);
b=b.replace(/[^A-Za-z0-9_]/g,"");
if(b.length>4){a=b
}}return a
};
treg.gya.initialize_comment_bar=function(){var a=$(".treg-gya-commenting-bar").first();
if(a.length==0){treg.logInfo("no commenting widget detected on page");
$("#hearst-commenting-bar-widget-screen-set").empty();
return
}a.append($("#hearst-commenting-bar-widget-screen-set"));
$("#hearst-commenting-bar-widget-screen-set").removeClass("gya-hide")
};
treg.gya.updateUserNameInHtml=function(){$(".treg-gya-displayname").html(treg.identity.displayName)
};
treg.gya.showNoProfileEditor=function(){if(!treg.gya.hasProfileEditor){$(".treg-no-profile-editor").show();
$(".treg-gya-onprofile-page").hide()
}else{$(".treg-no-profile-editor").hide();
$(".treg-gya-onprofile-page").show()
}};
treg.gya.hookUpLinks=function(){$(".treg-gya-manage-subscription-link").each(function(){this.href=treg.manageSubscriptionLink
});
$(".treg-gya-subscribe-link").each(function(){this.href=treg.subscribeLink
});
$(".treg-gya-subscribe-lookup-link").each(function(){var a=treg.subscribeLookupLink;
if(treg.gya.redirect_url!=null){a=a+"?returnUrl="+treg.gya.redirect_url
}this.href=a
});
$(".treg-gya-mangage-profile-link").each(function(){this.href=treg.profileLink
});
$(".treg-gya-activate-subscription-link").each(function(){this.href=treg.activateSubscriptionLink
});
$(".treg-gya-logout").click(function(){treg.endSession()
});
$(".treg-gya-privacy-link").each(function(){this.href=treg.privacyLink
});
$(".treg-gya-termsofservice-link").each(function(){this.href=treg.termsLink
});
$(".treg-gya-stnewsletter-link").each(function(){try{if(treg.sailthruNewsletterLink==null){var c="http://link.sfgate.com/manage/preferences?email={email}&hash={hash}";
var b=c.replace("{email}",encodeURIComponent(treg.identity.email));
treg.sailthruNewsletterLink=b.replace("{hash}",encodeURIComponent(treg.md5(treg.identity.email+"81daded6cf29249acfa97b5c69defd84")))
}this.href=treg.sailthruNewsletterLink
}catch(a){treg.logInfo("Error during treg-gya-stnewsletter-link setting")
}})
};
treg.gya.setup_labels=function(){$(".treg-gya-premium-site-name").html("San Francisco Chronicle");
$(".treg-gya-premium-site-name-abbrev").html("SF Chronicle");
$(".treg-gya-free-site-name").html("SFGate")
};
treg.gya.showHideHasSiteLogin=function(){if(treg.hasActiveSession()){if(treg.gya.accountInfo.loginProvider=="site"){$(".treg-has-site-login").show()
}else{$(".treg-has-site-login").hide()
}}};
treg.gya.showHideSubscriberAndNot=function(){if(treg.hasActiveSession()){if(treg.identity.edbId!=null){$(".treg-is-not-subscriber").hide();
$(".treg-is-subscriber").show()
}else{$(".treg-is-subscriber").hide();
$(".treg-is-not-subscriber").show()
}}else{$(".treg-is-not-subscriber").hide();
$(".treg-is-subscriber").hide()
}};
treg.gya.showHidePremiumFree=function(){var a=(window.location.href.indexOf("profile")>-1);
if(!treg.isPremium){$(".treg-is-premium").hide();
$(".treg-is-free").show();
if(a){$(".treg-gya-onprofile-page-free").show();
$(".treg-gya-onprofile-page-premium").hide()
}}else{$(".treg-is-free").hide();
$(".treg-is-premium").show();
if(a){$(".treg-gya-onprofile-page-free").hide();
$(".treg-gya-onprofile-page-premium").show()
}}};
treg.gya.showHideLoggedIn=function(){if(!treg.hasActiveSession()){$(".treg-not-logged-in").show();
$(".signInLink").show();
$(".treg-logged-in").hide();
$(".signOutLink").hide()
}else{$(".signOutLink").show();
$(".treg-logged-in").show();
$(".treg-not-logged-in").hide();
$(".signInLink").hide()
}if($(".janrain-comment-bar").length){if($("treg_captureSignInLink").hasClass("active")){$(".treg-toggle").show()
}else{$(".treg-toggle").hide()
}}};
treg.gya.updateMarkupExtensions=function(){treg.gya.setup_labels();
treg.gya.showHidePremiumFree();
treg.gya.showHideSubscriberAndNot();
treg.gya.hookUpLinks();
treg.gya.showNoProfileEditor();
treg.gya.updateUserNameInHtml();
treg.gya.showHideLoggedIn();
treg.gya.showHideHasSiteLogin()
};
treg.patchGigya=function(){treg.logInfo("patchGigya for forgot password bug");
try{(function(b){if(!b){throw"Gigya SDK is required, please initialize gigya.js first."
}var c=false;
var d=b.accounts.showScreenSet;
b.accounts.showScreenSet=function(){if(c===false){var f=b.utils.object.merge([b.thisScript.globalConf,arguments]);
var e=f.onBeforeScreenLoad;
f.onBeforeScreenLoad=function(){c=true;
var g=b._.plugins.ScreenSet.BaseForm;
var h=b._.plugins.ScreenSet.ResetPasswordForm;
h.prototype.linkInstanceElement=function(i){g.prototype.linkInstanceElement.call(this,i);
this._screenSet.data.finalizeRegistrationNeeded=false
};
if(e){return e.apply(this,arguments)
}};
return d.call(this,f)
}return d.apply(this,arguments)
}
})(window.gigya)
}catch(a){if(typeof(console)==="object"&&console.error){console.error("Failed to initialize temporary fix to bug 39911.");
console.error(a)
}}};
treg.registerEvent(treg.event.onInitialSessionState,function(){treg.logInfo("onInitialSessionState");
treg.gya.initialize_comment_bar();
treg.gya.initialize_profile_widget();
treg.gya.initialize_login_widgets();
treg.gya.initialize_premium_login_widgets();
treg.gya.updateMarkupExtensions();
treg.onReady&&treg.onReady()
});
treg.registerEvent(treg.event.on_dom_loaded,function(){treg.logInfo("dom loaded")
});
treg.registerEvent(treg.event.onScreenRendered,function(){treg.gya.updateMarkupExtensions()
});
treg.registerEvent(treg.event.onSessionFound,function(){treg.gya.showHideLoggedIn();
treg.gya.showHideSubscriberAndNot()
});
treg.registerEvent(treg.event.onSessionEnd,function(){treg.gya.showHideLoggedIn();
treg.gya.showHideSubscriberAndNot();
$(".treg-is-subscriber").hide();
if(treg.gya.hasProfileEditor){document.location.href="/"
}});
treg.gya.initCustomMessaging=function(){try{var b=gigya.i18n["gigya.services.accounts.plugins.screenSet.js"]["en"];
b["password_does_not_meet_complexity_requirements"]="Passwords must be at least 8 characters and contain both upper and lowercase letters and at least one number.";
b["passwords_do_not_match"]="The new passwords do not match";
b["email_already_exists"]="An account already exists with this email address.  Enter a different email address";
b["email_address_is_invalid"]="Email address is invalid, enter an email address with the correct format."
}catch(a){}};
treg.logInfo("main treg gigya script loaded");
treg.modules.gigya_analytics=treg.modules.gigya_analytics||{};
(function(){var b=this;
var a=treg.modules.gigya_analytics;
b.socialProvider="";
b.initializeModule=function(){treg.logInfo("initializing gigya analytics module");
a.gigyaID=null;
a.janrainID=null;
a.edbID=null;
a.socialNetwork=null;
a.automaticLogin=true;
a.pageType=treg.pageType;
a.pageMode="content";
a.lastLoginMethod="";
a.currentLoginMethod="";
a.currentLoginMethod=a.lastLoginMethod
};
treg.registerEvent(treg.event.onSessionFound,function(){a.gigyaID=treg.identity.id;
a.janrainID=treg.identity.janrainId;
a.edbID=treg.identity.edbId;
a.currentLoginMethod=treg.identity.currentLoginMethod;
if(a.automaticLogin&&a.lastLoginMethod!="traditionalSignin"){a.socialNetwork=b.socialProvider
}treg.fireEvent(treg.event.onAnalyticsDataUpdated)
});
treg.registerEvent(treg.event.onSessionNotFound,function(){a.automaticLogin=false;
a.janrainID=treg.identity.id;
a.edbID=treg.identity.edbId;
a.currentLoginMethod="traditionalSignin";
treg.fireEvent(treg.event.onAnalyticsDataUpdated)
});
b.initializeModule()
})();
treg.registerEvent(treg.event.onScreenRendered,function(a){if(typeof(a)=="undefined"){return
}var b=navigator.userAgent||navigator.vendor||window.opera;
if((b.indexOf("FBAN")>-1)||(b.indexOf("FBAV")>-1)||(b.indexOf("Twitter")>-1)){$("#"+a.sourceContainerID+" .gigya-social-login-container").hide();
$("#"+a.sourceContainerID+" .gigya-social-login").hide();
$("#"+a.sourceContainerID+" .gya-rule").hide()
}});
(function(){var k=function(r,q){var m=r[0],n=r[1],o=r[2],p=r[3];
m=c(m,n,o,p,q[0],7,-680876936);
p=c(p,m,n,o,q[1],12,-389564586);
o=c(o,p,m,n,q[2],17,606105819);
n=c(n,o,p,m,q[3],22,-1044525330);
m=c(m,n,o,p,q[4],7,-176418897);
p=c(p,m,n,o,q[5],12,1200080426);
o=c(o,p,m,n,q[6],17,-1473231341);
n=c(n,o,p,m,q[7],22,-45705983);
m=c(m,n,o,p,q[8],7,1770035416);
p=c(p,m,n,o,q[9],12,-1958414417);
o=c(o,p,m,n,q[10],17,-42063);
n=c(n,o,p,m,q[11],22,-1990404162);
m=c(m,n,o,p,q[12],7,1804603682);
p=c(p,m,n,o,q[13],12,-40341101);
o=c(o,p,m,n,q[14],17,-1502002290);
n=c(n,o,p,m,q[15],22,1236535329);
m=d(m,n,o,p,q[1],5,-165796510);
p=d(p,m,n,o,q[6],9,-1069501632);
o=d(o,p,m,n,q[11],14,643717713);
n=d(n,o,p,m,q[0],20,-373897302);
m=d(m,n,o,p,q[5],5,-701558691);
p=d(p,m,n,o,q[10],9,38016083);
o=d(o,p,m,n,q[15],14,-660478335);
n=d(n,o,p,m,q[4],20,-405537848);
m=d(m,n,o,p,q[9],5,568446438);
p=d(p,m,n,o,q[14],9,-1019803690);
o=d(o,p,m,n,q[3],14,-187363961);
n=d(n,o,p,m,q[8],20,1163531501);
m=d(m,n,o,p,q[13],5,-1444681467);
p=d(p,m,n,o,q[2],9,-51403784);
o=d(o,p,m,n,q[7],14,1735328473);
n=d(n,o,p,m,q[12],20,-1926607734);
m=g(m,n,o,p,q[5],4,-378558);
p=g(p,m,n,o,q[8],11,-2022574463);
o=g(o,p,m,n,q[11],16,1839030562);
n=g(n,o,p,m,q[14],23,-35309556);
m=g(m,n,o,p,q[1],4,-1530992060);
p=g(p,m,n,o,q[4],11,1272893353);
o=g(o,p,m,n,q[7],16,-155497632);
n=g(n,o,p,m,q[10],23,-1094730640);
m=g(m,n,o,p,q[13],4,681279174);
p=g(p,m,n,o,q[0],11,-358537222);
o=g(o,p,m,n,q[3],16,-722521979);
n=g(n,o,p,m,q[6],23,76029189);
m=g(m,n,o,p,q[9],4,-640364487);
p=g(p,m,n,o,q[12],11,-421815835);
o=g(o,p,m,n,q[15],16,530742520);
n=g(n,o,p,m,q[2],23,-995338651);
m=h(m,n,o,p,q[0],6,-198630844);
p=h(p,m,n,o,q[7],10,1126891415);
o=h(o,p,m,n,q[14],15,-1416354905);
n=h(n,o,p,m,q[5],21,-57434055);
m=h(m,n,o,p,q[12],6,1700485571);
p=h(p,m,n,o,q[3],10,-1894986606);
o=h(o,p,m,n,q[10],15,-1051523);
n=h(n,o,p,m,q[1],21,-2054922799);
m=h(m,n,o,p,q[8],6,1873313359);
p=h(p,m,n,o,q[15],10,-30611744);
o=h(o,p,m,n,q[6],15,-1560198380);
n=h(n,o,p,m,q[13],21,1309151649);
m=h(m,n,o,p,q[4],6,-145523070);
p=h(p,m,n,o,q[11],10,-1120210379);
o=h(o,p,m,n,q[2],15,718787259);
n=h(n,o,p,m,q[9],21,-343485551);
r[0]=a(m,r[0]);
r[1]=a(n,r[1]);
r[2]=a(o,r[2]);
r[3]=a(p,r[3])
};
var b=function(o,m,n,u,p,r){m=a(a(m,o),a(u,r));
return a((m<<p)|(m>>>(32-p)),n)
};
var c=function(m,n,o,p,u,q,r){return b((n&o)|((~n)&p),m,n,u,q,r)
};
var d=function(m,n,o,p,u,q,r){return b((n&p)|(o&(~p)),m,n,u,q,r)
};
var g=function(m,n,o,p,u,q,r){return b(n^o^p,m,n,u,q,r)
};
var h=function(m,n,o,p,u,q,r){return b(o^(n|(~p)),m,n,u,q,r)
};
var i=function(p){txt="";
var o=p.length,q=[1732584193,-271733879,-1732584194,271733878],m;
for(m=64;
m<=p.length;
m+=64){k(q,j(p.substring(m-64,m)))
}p=p.substring(m-64);
var r=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
for(m=0;
m<p.length;
m++){r[m>>2]|=p.charCodeAt(m)<<((m%4)<<3)
}r[m>>2]|=128<<((m%4)<<3);
if(m>55){k(q,r);
for(m=0;
m<16;
m++){r[m]=0
}}r[14]=o*8;
k(q,r);
return q
};
var j=function(o){var n=[],m;
for(m=0;
m<64;
m+=4){n[m>>2]=o.charCodeAt(m)+(o.charCodeAt(m+1)<<8)+(o.charCodeAt(m+2)<<16)+(o.charCodeAt(m+3)<<24)
}return n
};
var f="0123456789abcdef".split("");
var l=function(o){var p="",m=0;
for(;
m<4;
m++){p+=f[(o>>(m*8+4))&15]+f[(o>>(m*8))&15]
}return p
};
var e=function(n){for(var m=0;
m<n.length;
m++){n[m]=l(n[m])
}return n.join("")
};
treg.md5=function(m){return e(i(m))
};
var a=function(m,n){return(m+n)&4294967295
};
if(treg.md5("hello")!="5d41402abc4b2a76b9719d911017c592"){var a=function(o,p){var m=(o&65535)+(p&65535),n=(o>>16)+(p>>16)+(m>>16);
return(n<<16)|(m&65535)
}
}})();
(function(){var a="treg_redirect_refresh";
var b=function(e,h){if(!h){h=window.location.href
}e=e.replace(/[\[\]]/g,"\\$&");
var f=new RegExp("[?&]"+e+"(=([^&#]*)|&|#|$)"),g=f.exec(h);
if(!g){return null
}if(!g[2]){return null
}return decodeURIComponent(g[2].replace(/\+/g," "))
};
var c=function(h){var f=document.cookie.split("; ");
for(var g=0;
g<f.length;
g++){var e=f[g].split("=");
if(e[0]===h){return true
}}return false
};
var d=b("return",window.location.search);
if(d==null){d=b("returnUrl",window.location.search)
}if(d!=null){treg.logInfo("setting up redirect");
treg.gya.redirect_url=d;
treg.registerEvent(treg.event.onBeforeProfileWidget,function(){if(treg.hasActiveSession()){document.cookie=a+"=1; path=/";
treg.isRedirecting=true;
window.location.href=d
}})
}if(c(a)){document.cookie=a+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/";
treg.reloadPage("redirect refresh")
}})();
treg.fireEvent(treg.event.on_treg_loaded);$(document).ready(function(){treg.logInfo("initializing ux.js");
$(".gya-menu, .gya-title, .gya-content, .gya-toggle, #treg_captureSignInLink").on("click",function(d){d.stopPropagation()
});
$(".gya-title").click(function(){$(".gya-title").removeClass("on");
$(".gya-content").slideUp("normal");
treg.gya.revertShowPasswordCheckboxes();
if($(this).siblings(".gya-content").is(":hidden")==true){if($(this).hasClass("expandable")){$(this).addClass("on");
$(this).siblings(".gya-content").slideDown("normal")
}}});
var c="ontouchstart" in document.documentElement;
console.log("isTouchDevice = "+c);
if(c==true){$(document).on("touchstart",".gya-info",function(){event.stopPropagation();
if($(this).hasClass("tip-on")){$(this).siblings(".gya-info-text").removeClass("info-on");
$(this).removeClass("tip-on");
console.log("Info off")
}else{$(this).siblings(".gya-info-text").addClass("info-on");
$(this).addClass("tip-on");
console.log("Info on")
}});
if($(".tip-on").length>0){$(document).on("touchstart","body",function(){$(".gya-info").removeClass("tip-on");
$(".gya-info-text").removeClass("info-on")
})
}}else{$(document).on("mouseenter",".gya-info",function(){$(this).siblings(".gya-info-text").addClass("info-on");
$(this).addClass("tip-on")
}).on("mouseleave",".gya-info",function(){$(this).siblings(".gya-info-text").removeClass("info-on");
$(this).removeClass("tip-on")
})
}function b(){if($("#treg_captureSignInLink").hasClass("active")){$(".gya-toggle").hide();
$("#treg_captureSignInLink").removeClass("active");
treg.gya.revertShowPasswordCheckboxes()
}else{$(".gya-toggle").show();
$("#treg_captureSignInLink").addClass("active")
}}function a(){var d=$("body").find(".comments-toggle");
var e=$("body").find(".gya-toggle");
if(d.hasClass("open")){e.hide();
d.removeClass("open");
treg.gya.revertShowPasswordCheckboxes()
}else{e.show();
d.addClass("open")
}}$("body").find(".comments-toggle").on("click",function(d){a();
d.stopPropagation()
});
$(document).click(function(f){var d=$(".gya-toggle");
var g=$(".gya-content");
if(!g.is(f.target)&&g.has(f.target).length===0){g.slideUp("normal");
$(".gya-title").removeClass("on");
treg.gya.revertShowPasswordCheckboxes(g)
}if(!d.is(f.target)&&d.has(f.target).length===0){d.hide();
$("#treg_captureSignInLink").removeClass("active");
$(".comments-toggle").removeClass("open");
treg.gya.revertShowPasswordCheckboxes(d)
}})
});