$(document).ready(function() {
  var position_target = $('.site_block');
  if ((window.location.pathname == '') || (window.location.pathname == '/') || (window.location.pathname == '/index.html')) {
    position_target = $('#site_block_home');
  }

  $('#browse_hover').qtip({
              content: browse_flyout_content,
              show: {
                      event: "mouseover"
              },
              hide: {
                      fixed: true,
                      delay: 300,
                      event: "mouseout"
              },
              position: {
                      my: "topCenter",
                      at: "topCenter",
                      target: position_target
              },
              style: {
                      tip: false,
                      border: 15,
                      width: "91%",
                      classes: "ui-tooltip-light ui-tooltip-rounded ui-tooltip-shadow"
              }
      });
});
