$(document).ready(function() {
  trunc_descriptions();
});

function trunc_descriptions() {
  var display_length = 650;
  if (window.location.pathname.indexOf('/browse') == 0) {
    display_length = 525;
  }
  $('.description').truncate({ max_length: display_length, more: "show more", less: "show less" });
}
