jQuery(function($) {
    $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });

    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });

    $('a[href="#content"]').on('click', function(event) {
        event.preventDefault();
        $('a[name="content"]')[0].scrollIntoView();
        window.scrollBy(0, -35);

    });
    $('.dropdown-menu option, .dropdown-menu select').click(function(e) {
        e.stopPropagation();
      });


    $('.thumbs-rating-up').on('click touchstart', function(event) {
      if($(this).parent('.thumbs-rating-container').children('span.thumbs-rating-already-voted')[0].style.display != "block") {
       $(this).addClass("clicked");
     }
    });
    $('.thumbs-rating-down').on('click touchstart', function(event) {
      if($(this).parent('.thumbs-rating-container').children('span.thumbs-rating-already-voted')[0].style.display != "block") {
       $(this).addClass("clicked");
       }
    });

    $(document).scroll(function () {
  var $nav = $(".navbar");
  $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
});

  $("a[href='#search']").parent().addClass("search-li");

    var $analyticsOff = $('.adsbygoogle:hidden');
    var $analyticsOn = $('.adsbygoogle:visible');

    $analyticsOff.each(function() {
      $(this).remove();
    });

});
